package pojos;

import java.util.List;

import dao.Dao;
import dao.IDao;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import query.Predicate;
import query.Query;

public class Product extends Dao<Product> implements IDao<Product> {
	
	private int id; 
	private int brand_id; 
	private int category_id; 
	private double sell_price; 
	private int stock; 
	
	private String brand_name; 
	private String category_name; 
	
	private final StringProperty brandName; 
	private final StringProperty categoryName; 
	private final DoubleProperty sellPrice; 
	
	public static String tableName = "product"; 
	
	public Product(){
		this(0, 0, 0, 0); 
	}; 
	
	public Product(int id, int brand_id, int category_id, double sell_price){ 
		super("id", "brand_id", "category_id", "sell_price"); 
		this.id = id; 
		this.brand_id = brand_id; 
		this.category_id = category_id; 
		this.sell_price = sell_price; 
		
		this.brandName = new SimpleStringProperty(); 
		this.categoryName = new SimpleStringProperty(); 
		this.sellPrice = new SimpleDoubleProperty(); 
	}
	
	public static int countAll(){
		Query q = new Query("select")
						.selectFields("COUNT(*)")
						.setTable(tableName); 
		
		int count = Dao.countAll(q); 
		
		return count; 
	} 
	
	@Override
	public void fetch(int id) {
		Product p = Dao.fetch(Product.class, tableName, id); 
		if(p != null){
			this.setId(id); 
			Brand b = new Brand(); 
			b.fetch(p.getBrand_id()); 
			this.setBrand_name(b.getName()); 
			
			Category c = new Category(); 
			c.fetch(p.getCategory_id()); 
			this.setCategory_name(c.getName()); 
			
			StockItem s = new StockItem(); 
			s.fetch(p.getId()); 
			this.setStock(s.getQuantity()); 
			
			this.setBrand_id(p.getBrand_id());
			this.setCategory_id(p.getCategory_id());  
			this.setSell_price(p.getSell_price());
			
		} 
	} 
	@Override
	public void save(){
		int id = super.save(tableName); 
		if( id > 0 ) this.id = id; 
	} 
	@Override
	public void delete(){
		super.delete(tableName); 
	} 
	
	/**
	 * Get a list of products based on a query string 
	 * @param fieldNames 
	 * @return List of Products 
	 */ 
	public static List<Product> fetchAll(int limit, int offset, String... fieldNames){ 
		
		//Specify custom query 
		Query q = new Query("select")
						.selectFields(fieldNames) 
						.setTable(tableName + " Product")
						.innerJoinOn(Brand.tableName + " Brand", "Product.brand_id = Brand.id")
						.innerJoinOn(Category.tableName + " Category", "Product.category_id = Category.id")
						.innerJoinOn(StockItem.tableName + " Stock", "Product.id = Stock.product_id")
						.orderBy("Brand.name", "Category.name")
						.limit(limit)
						.offset(offset); 
		
		List<Product> list = Dao.fetchAll(Product.class, q); 
		return list; 
	}
	
	public static List<Product> fetchByBrandLike(String strLike, int limit, int offset, String ...fieldNames){
		Predicate<String> p1 = new Predicate<>("Brand.name", "LIKE", "%" + strLike + "%"); 
		//Specify custom query 
		Query q = new Query("select")
				.selectFields(fieldNames) 
				.setTable(tableName + " Product")
				.innerJoinOn(Brand.tableName + " Brand", "Product.brand_id = Brand.id")
				.innerJoinOn(Category.tableName + " Category", "Product.category_id = Category.id")
				.where(p1)
				.orderBy("Brand.name", "Category.name")
				.limit(limit)
				.offset(offset); 
		
		List<Product> list = Dao.fetchAll(Product.class, q); 
		return list; 
	} 
	
	public static List<Product> fetchByCategoryLike(String strLike, int limit, int offset, String ...fieldNames){
		Predicate<String> p1 = new Predicate<>("Category.name", "LIKE", "%" + strLike + "%"); 
		//Specify custom query 
		Query q = new Query("select")
						.selectFields(fieldNames) 
						.setTable(tableName + " Product")
						.innerJoinOn(Brand.tableName + " Brand", "Product.brand_id = Brand.id")
						.innerJoinOn(Category.tableName + " Category", "Product.category_id = Category.id")
						.where(p1)
						.orderBy("Brand.name", "Category.name")
						.limit(limit)
						.offset(offset); 
		List<Product> list = Dao.fetchAll(Product.class, q); 
		return list; 
	}	
	
	public static int countByCategoryLike(String strLike){
		Predicate<String> p1 = new Predicate<>("Category.name", "LIKE", "%" + strLike + "%"); 
		Query q = new Query("select")
				.selectFields("COUNT(*)") 
				.setTable(tableName + " Product")
				.innerJoinOn(Brand.tableName + " Category", "Product.category_id= Category.id")
				.where(p1); 
		
		return Dao.countAll(q); 
	}
	
	public static int countByBrandLike(String strLike){
		Predicate<String> p1 = new Predicate<>("Brand.name", "LIKE", "%" + strLike + "%"); 
		Query q = new Query("select")
				.selectFields("COUNT(*)") 
				.setTable(tableName + " Product")
				.innerJoinOn(Brand.tableName + " Brand", "Product.brand_id = Brand.id")
				.where(p1); 
		
		return Dao.countAll(q); 
	}	
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getStock(){
		return stock; 
	}
	public void setStock(int stock){
		this.stock = stock; 
	}
	
	public int getBrand_id() {
		return brand_id;
	}
	public void setBrand_id(int brand_id) {
		this.brand_id = brand_id;
	}
	public void setBrand_name(String brand_name){
		this.brand_name = brand_name; 
		this.setBrandName(brand_name); 
	}
	public String getBrand_name(){
		return brand_name; 
	}
	public int getCategory_id() {
		return category_id;
	}
	public void setCategory_id(int category_id) {
		this.category_id = category_id;
	}
	public void setCategory_name(String category_name){
		this.category_name = category_name; 
		this.setCategoryName(category_name);
	}
	public String getCategory_name(){
		return category_name; 
	}
	public String getProductIdString(){
		return this.getId() + " - " + this.getBrand_name() + " - " + this.getCategory_name(); 
	}
	
	public double getSell_price() {
		return sell_price;
	}
	public void setSell_price(double sell_price) {
		this.sell_price = sell_price; 
		this.setSellPrice(sell_price); 
	}
	//Properties 
    public void setBrandName(String brandName) {
        this.brandName.set(brandName); 
    }
	public StringProperty brandNameProperty() {
        return brandName;
    }
    public void setCategoryName(String categoryName) { 
        this.categoryName.set(categoryName); 
    } 
	public StringProperty categoryNameProperty() {
        return categoryName;
    }
	public void setSellPrice(Double sellPrice){ 
		this.sellPrice.set(sellPrice); 
	}
	public DoubleProperty sellPriceProperty(){
		return sellPrice; 
	}
	
	@Override
	public String toString() { 
		return "Product [id=" + id + ", brand_id=" + brand_id + ", category_id=" + category_id + ", sell_price="
				+ sell_price + ", brand_name=" + brandName + ", category_name=" + categoryName
				+ ", categoryName=" + categoryName + "]";
	} 
	
}
