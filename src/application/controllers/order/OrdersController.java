package application.controllers.order;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import application.VistaNavigator;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.layout.Pane;

public class OrdersController { 
	
	@FXML private Pane pane_order_fragment; 
	@FXML private Button btn_dashboard; 
	
	private Parent viewOrderBook; 
	private Parent viewAddOrder; 
	
	OrderBookController orderBookController; 
	AddOrderController addOrderController; 
	
    private List<Parent> viewStack = new ArrayList<>();     
	
	public void initialize(){
		this.showOrderBook(); 
	}
	
	/**
	 * Navigate to dashboard
	 * @param event
	 */
	@FXML void gotoDashboard(ActionEvent event) {
       VistaNavigator.loadVista(VistaNavigator.DASHBOARD);
	}
	
    /**
     * Manage detail pane view stack to avoid duplicate view instances when switching views 
     * @param view
     */
    private void showView(Parent view){  
    	if(viewStack.size() > 0){ 
    		pane_order_fragment.getChildren().removeAll(viewStack); 
    	}
		pane_order_fragment.getChildren().add(view); 
		viewStack.add(view); 
    }
    
    //The following methods are public because they are accessible from the fragments 
    /**
     * Show the order book fragment 
     */
    public void showOrderBook(){ 
    	if(viewOrderBook == null){
    		try { 
    			FXMLLoader loader= new FXMLLoader(getClass().getResource("/application/resources/Order_book.fxml")); 
    			viewOrderBook = loader.load(); 
    			
    			orderBookController = (OrderBookController) loader.getController(); 
    			orderBookController.setParentController(this); 
    			
    		} catch (IOException e) {
    			e.printStackTrace();
    		} 
    	}
		showView(viewOrderBook); 
    }
    
    /**
     * Show the addOrder fragment 
     */
    public void showAddOrder(){
		try { 
			FXMLLoader loader= new FXMLLoader(getClass().getResource("/application/resources/Order_add.fxml")); 
			viewAddOrder = loader.load(); 
			showView(viewAddOrder); 
			
			addOrderController = (AddOrderController) loader.getController(); 
			addOrderController.setParentController(this); 
			
		} catch (IOException e) {
			e.printStackTrace();
		}    	
    	
    }
    
}
