package utils;

public class TypeUtils {
	
	/**
	 * Checks if String is numeric 
	 * @param strNumber
	 * @return
	 */
	public static boolean isNumeric(String strNumber){
		  try {  
		    Double.parseDouble(strNumber); 
		    
		  }catch(NumberFormatException nfe){  
		    return false;  
		  }  
		  return true; 		
	}
}
