package application.controllers.order;

import java.util.ArrayList;
import java.util.List;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.Pagination;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import pojos.Order;
import pojos.Product;
import pojos.StockItem;

public class WizardPlaceOrdersController {
	
    @FXML private TableView<Product> productsTable;
    @FXML private TableColumn<Product, Integer> colProductId;
    @FXML private TableColumn<Product, String> colBrand;
    @FXML private TableColumn<Product, String> colCategory;
    @FXML private TableColumn<Product, Integer> colStock;
    @FXML private Pagination pagination; 
    
    @FXML private TableView<Order> addedOrdersTable;
    @FXML private TableColumn<Order, String> colProductString; 
    @FXML private TableColumn<Order, Integer> colQuantity; 
    @FXML private TableColumn<Order, Double> colPrice; 
    
    @FXML private ComboBox<String> cbb_product_search_by; 
    @FXML private Button btn_addOrder; 
    @FXML private Button btn_deleteOrder; 
    @FXML private Button btn_clearOrders; 
    @FXML private Button btn_previous; 
    @FXML private Button btn_next; 
    @FXML private TextField txt_product_search; 
    @FXML private TextField txt_quantity; 
    @FXML private Label lbl_total_price; 
    @FXML private Label lbl_placeholder_added; 
    
	AddOrderController parentController; 
	
    private static int RESULTS_PER_PAGE = 11; 
    public static final int TABLE_STATE_ALL = 1; 
    public static final int TABLE_STATE_SEARCH_BY_PRODUCT_ID = 2; 
    public static final int TABLE_STATE_SEARCH_BY_BRAND = 3; 
    public static final int TABLE_STATE_SEARCH_BY_CATEGORY = 4; 
    public static int TABLE_STATE = TABLE_STATE_ALL; 
    public static String SEARCH_STRING; 
    	
	private Product activeProduct; 
	private Order activeOrder; 
	private Order orderToAdd; 
	
	public void initialize(){ 
		
		//Set product table column values 
		colProductId.setCellValueFactory(new PropertyValueFactory<Product, Integer>("id"));
		colBrand.setCellValueFactory(new PropertyValueFactory<Product, String>("brandName"));
		colCategory.setCellValueFactory(new PropertyValueFactory<Product, String>("categoryName"));
		colStock.setCellValueFactory(new PropertyValueFactory<Product, Integer>("stock"));
		
		//Set added orders table column values 
		colProductString.setCellValueFactory(new PropertyValueFactory<Order, String>("productIdString")); 
		colQuantity.setCellValueFactory(new PropertyValueFactory<Order, Integer>("quantity")); 
		colPrice.setCellValueFactory(new PropertyValueFactory<Order, Double>("orderValuePp")); 
		
        //Add client combobox sort by change handler 
        cbb_product_search_by.getSelectionModel().selectedItemProperty().addListener((observableValue, oldValue, newValue) -> {
        	resetProductsTable(); 
        	if(newValue.equals("id")){
        		txt_product_search.setPromptText("Enter client id"); 
        	} else if (newValue.equals("brand")) {
        		txt_product_search.setPromptText("Enter brand name"); 
        	} else if (newValue.equals("category")) {
        		txt_product_search.setPromptText("Enter category name"); 
        	}
        	
        }); 
        
        //Add table select listener products  
        productsTable.getSelectionModel().selectedItemProperty().addListener((observableValue, oldValue, newValue) -> {
            if (productsTable.getSelectionModel().getSelectedItem() != null) { 
            	activeProduct = newValue; 
            	if(orderToAdd == null) orderToAdd = new Order(); 
            	//Set activeOrder properties 
            	orderToAdd.setProduct_id(newValue.getId()); 
            	orderToAdd.setProductIdString(activeProduct.getProductIdString()); 
            	
            	if(!txt_quantity.getText().equals("") && utils.TypeUtils.isNumeric(txt_quantity.getText())){
            		if(Integer.parseInt(txt_quantity.getText()) > 0)
            			btn_addOrder.setDisable(false);
            	}else{
            		btn_addOrder.setDisable(true);
            	}
            }
        }); 
        
        //Add table select listener added orders 
        addedOrdersTable.getSelectionModel().selectedItemProperty().addListener((observableValue, oldValue, newValue) -> {
            if (addedOrdersTable.getSelectionModel().getSelectedItem() != null) { 
            	activeOrder = newValue; 
            	btn_deleteOrder.setDisable(false);
            } 
        }); 
        
        //Add change listener to quantity field 
        txt_quantity.textProperty().addListener((observableValue, oldValue, newValue) -> {
        	if(!newValue.equals("") && utils.TypeUtils.isNumeric(newValue)){ 
        		if(Integer.parseInt(newValue) > 0 && activeProduct != null){
            		btn_addOrder.setDisable(false); 
            		if(orderToAdd == null) orderToAdd = new Order(); 
            		orderToAdd.setQuantity(Integer.parseInt(txt_quantity.getText()));
        		} else if(Integer.parseInt(newValue) == 0) btn_addOrder.setDisable(true);; 
        	} else {
        		btn_addOrder.setDisable(true);
        	}
        });
		
	}
	
	//Set parent controllers for back navigation 
	/**
	 * Set the parent controller for wizard navigation 
	 * @param parentController
	 */
	public void setParentController(AddOrderController parentController){
		this.parentController = parentController; 
		btn_addOrder.setDisable(true); 
		btn_deleteOrder.setDisable(true);
		resetProductsTable();
		
		//Pagination listener 
        pagination.currentPageIndexProperty().addListener((observableValue, oldValue, newValue) -> { 
        	showProducts(RESULTS_PER_PAGE * (int) newValue); 
        }); 
	}
	
    /**
     * Reset the addOrders table, without filter (first table) 
     */
	@FXML private void resetProductsTable(){ 
		activeProduct = null; 
		TABLE_STATE = TABLE_STATE_ALL; 
		
		setSearchText(""); 
		txt_quantity.setText("");
		btn_addOrder.setDisable(true);
		
		int count = Product.countAll(); 
		pagination.setPageCount(count / RESULTS_PER_PAGE + 1); 
		showProducts(0); 
	} 
		
	/**
	 * Rebuild added orders table 
	 */
	public void resetAddedOrdersTable(){
		List<Order> list = this.parentController.getOrders(); 
		lbl_total_price.setText(String.valueOf(calcTotalPrice())); 
		
		if(list.size() > 0){
			btn_clearOrders.setDisable(false);
			btn_next.setDisable(false);
		} else { 
			//Disable button if there are no items to clear 
			btn_clearOrders.setDisable(true);
			//Disable next button if there aren't any orders to add 
			btn_next.setDisable(true);
		} 
		
		addedOrdersTable.getItems().setAll(list); 
	}
	
	/**
	 * Clear added orders table 
	 */
	@FXML public void clearOrders(){
		this.parentController.resetOrders(); 
		this.resetAddedOrdersTable();
	}
	
	@FXML public void deleteOrder(){
		this.parentController.deleteOrder(activeOrder);
		resetAddedOrdersTable();
	}
	
	/**
	 * Add deep copy of order to added orders list 
	 */
	@FXML public void addOrder(){ 
		Order order = new Order(); 
		//Set client on order 
		order.setClient_id(this.parentController.getClient().getId());
		order.setProduct_id(orderToAdd.getProduct_id());
		order.setQuantity(orderToAdd.getQuantity());
		order.setProductIdString(orderToAdd.productIdStringProperty().get());
		order.setOrder_value_pp(activeProduct.getSell_price());
		
		//Add to added orders list 
		this.parentController.addOrder(order); 
		orderToAdd = null; 
		//Clear fields 
		txt_quantity.setText("");
		//Update added orders table 
		resetAddedOrdersTable(); 
	}
	
	/**
	 * Calculate price of the total order
	 */
	private double calcTotalPrice(){
		double totalPrice = 0; 
		for (Order order : this.parentController.getOrders()) {
			totalPrice += (order.getOrderValuePp() * order.getQuantity()); 
		}
		return totalPrice; 
	}
	
	/**
	 * Public method to set searchbox text 
	 * @param strText
	 */
    public void setSearchText(String strText){
    	txt_product_search.setText(strText); 
    }
    
	/**
	 * Navigate to selectClients 
	 */
	@FXML public void gotoSelectClient(){
		this.parentController.gotoSelectClient();
	}
	
	/**
	 * Navigate to saveOrders 
	 */
	@FXML public void gotoSaveOrders(){
		this.parentController.gotoSaveOrders();
	}
	
	/**
	 * Manage search box key events 
	 * @param event
	 */
	@FXML public void onKeyReleasedSearchClient(KeyEvent event){ 
		if((event.getCode() == KeyCode.BACK_SPACE || event.getCode() == KeyCode.DELETE)){ 
			if(txt_product_search.getText().length() == 0){ 
				resetProductsTable(); 
			}
		}else if (event.getCode() == KeyCode.ENTER) {
			searchProduct(); 
		} 
	} 
	
	/**
	 * Searchbox product
	 */
	@FXML public void searchProduct(){
		String strSearch = txt_product_search.getText().trim(); 
		String strSearchBy = ((String) cbb_product_search_by.getSelectionModel().getSelectedItem().toString()).trim(); 
		
		SEARCH_STRING = strSearch; 
		switch(strSearchBy){ 
			
			case "id": 
				TABLE_STATE = TABLE_STATE_SEARCH_BY_PRODUCT_ID; 
				break; 
			
			case "category": 
				TABLE_STATE = TABLE_STATE_SEARCH_BY_CATEGORY; 
				break; 
			
			case "brand": 
				TABLE_STATE = TABLE_STATE_SEARCH_BY_BRAND; 
				break; 
		}
		showProducts(0);	
	}
	
	/**
	 * Show orders depending on state 
	 */
	public void showProducts(int pageOffset){ 
		List<Product> list = null;  
		
		if(TABLE_STATE == TABLE_STATE_ALL){ 
			list = getProductList(pageOffset); 
			if(pageOffset == 0) pagination.setCurrentPageIndex(0); 
			
		} else if(TABLE_STATE == TABLE_STATE_SEARCH_BY_PRODUCT_ID){ 
			int productId = SEARCH_STRING.equals("") ? 0 : Integer.parseInt(SEARCH_STRING); 
			list = getProductListSearchById(productId); 
			pagination.setPageCount(1); 
			
		} else if(TABLE_STATE == TABLE_STATE_SEARCH_BY_BRAND){
			list = getProductListSearchByBrand(SEARCH_STRING, pageOffset); 
			pagination.setPageCount(Product.countByBrandLike(SEARCH_STRING) / RESULTS_PER_PAGE + 1); 
			
		} else if(TABLE_STATE == TABLE_STATE_SEARCH_BY_CATEGORY){
			list = getProductListSearchByCategory(SEARCH_STRING, pageOffset); 
			pagination.setPageCount(Product.countByCategoryLike(SEARCH_STRING) / RESULTS_PER_PAGE + 1); 
			
		}
		
		list = localUpdateStock(list); 
		productsTable.getItems().setAll(list); 
	}
	
	/**
	 * Recalculate local product list stock, taken into account the added products without db sync 
	 */
	private List<Product> localUpdateStock(List<Product> list){ 
		List<Order> orderList = this.parentController.getOrders(); 
		//If the product is found, reduce the local stock number 
		for (Order o : orderList) {
			for (Product p : list) {
				if(o.getProduct_id() == p.getId()){
					p.setStock(p.getStock() - o.getQuantity());
				}
			}
		}
		return list; 
	}
	
	/**
	 * Get all products 
	 * @param pageOffset
	 * @return
	 */
	public List<Product> getProductList(int pageOffset){
		List<Product> list = Product.fetchAll(RESULTS_PER_PAGE, pageOffset, "product.*", "brand.name as brand_name", "category.name as category_name", "stock.quantity as stock"); 
		return list; 
	} 
	
    /**
     * Fetch orders by client id
     * @param Clientid
     * @return
     */
    private List<Product> getProductListSearchById(int productId){
    	List<Product> list = new ArrayList<>(); 
    	Product product = new Product(); 
    	product.fetch(productId); 
    	if(product.getId() > 0)
    		list.add(product); 
    	return list; 
    }
    
    /**
     * Get products by category name 
     * @param strSearch
     * @param offset
     * @return
     */
    private List<Product> getProductListSearchByCategory(String strSearch, int offset){
    	List<Product> list = Product.fetchByCategoryLike(strSearch, RESULTS_PER_PAGE, offset, "Product.*", "Brand.name as brand_name", "Category.name as category_name"); 
    	list = getStock(list); 
    	return list; 
    } 
	
    /**
     * Get products by brand name
     * @param strSearch
     * @param offset
     * @return
     */
    private List<Product> getProductListSearchByBrand(String strSearch, int offset){
    	List<Product> list = Product.fetchByBrandLike(strSearch, RESULTS_PER_PAGE, offset, "Product.*", "Brand.name as brand_name", "Category.name as category_name"); 
    	list = getStock(list); 
    	return list; 
    } 
    
    /**
     * Get stock value for product list 
     * @param list
     * @return
     */
    private List<Product> getStock(List<Product> list){
    	for (Product p : list) {
    		//Get the stock 
			StockItem stock = new StockItem(); 
			stock.fetch(p.getId()); 
			p.setStock(stock.getQuantity()); 
		}
    	return list; 
    }
    
}
