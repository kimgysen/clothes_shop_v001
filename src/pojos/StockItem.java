package pojos;

import java.util.List;

import dao.Dao;
import dao.IDao;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import query.Predicate;
import query.Query;

public class StockItem extends Dao<StockItem> implements IDao<StockItem>{
	
	private int product_id; 
	private int brand_id; 
	private int category_id; 
	private String brand_name; 
	private String category_name; 
	private int quantity; 
	private double sell_price; 
	//Add timestamp later on 
	
	//Properties for table 
	private final IntegerProperty productId; 
	private final StringProperty brandName; 
	private final StringProperty categoryName; 
	private final IntegerProperty productQuantity; 
	private final DoubleProperty sellPrice; 
	
	public static String tableName = "stock"; 
	
	public StockItem(){
		this(0, 0, 0, 0, 0); 
	}
	
	public StockItem(int product_id, int brand_id, int category_id, int quantity, double sellPrice){ 
		super("product_id", "brand_id", "category_id", "quantity", "sell_price"); 
		this.product_id = product_id; 
		this.brand_id = brand_id; 
		this.category_id = category_id; 
		this.quantity = quantity; 
		this.sell_price = sellPrice; 
		
		this.productId = new SimpleIntegerProperty(); 
		this.brandName = new SimpleStringProperty(); 
		this.categoryName = new SimpleStringProperty(); 
		this.productQuantity = new SimpleIntegerProperty(); 
		this.sellPrice = new SimpleDoubleProperty(); 
	}
	
	public static int countAll(){
		Query q = new Query("select")
						.selectFields("COUNT(*)")
						.setTable(tableName); 
		
		int count = Dao.countAll(q); 
		
		return count; 
	} 
	
	@Override
	public void fetch(int product_id) { 
		StockItem s = Dao.fetch(StockItem.class, tableName, product_id, "product_id"); 
		if(s != null){
			this.setProduct_id(s.getProductId());
			this.setQuantity(s.getQuantity());
		}
	}
	
	@Override
	public void save() { 
		int id = super.save(tableName, "product_id"); 
		if( id > 0 ) this.product_id = id; 
	}

	@Override
	public void delete() {
		super.delete(tableName, true); 
	}
	
	/**
	 * Get a list of stock items based on a query string 
	 * @param fieldNames 
	 * @return List of stock items 
	 */ 
	public static List<StockItem> fetchAll(int limit, int offset, String... fieldNames){ 
		
		//Specify custom query 
		Query q = new Query("select")
						.selectFields(fieldNames) 
						.setTable(StockItem.tableName + " Stock")
						.innerJoinOn(Product.tableName + " Product", "Stock.product_id = Product.id")
						.innerJoinOn(Brand.tableName + " Brand", "Product.brand_id = Brand.id")
						.innerJoinOn(Category.tableName + " Category", "Product.category_id = Category.id")
						.orderBy("Brand.name", "Category.name")
						.limit(limit)
						.offset(offset); 
		
		List<StockItem> list = Dao.fetchAll(StockItem.class, q); 
		return list; 
	}
	
	public static List<StockItem> fetchByBrandLike(String strLike, int limit, int offset, String ...fieldNames){
		Predicate<String> p1 = new Predicate<>("Brand.name", "LIKE", "%" + strLike + "%"); 
		//Specify custom query 
		Query q = new Query("select")
				.selectFields(fieldNames) 
				.setTable(StockItem.tableName + " Stock")
				.innerJoinOn(Product.tableName + " Product", "Stock.product_id = Product.id" )
				.innerJoinOn(Brand.tableName + " Brand", "Product.brand_id = Brand.id")
				.innerJoinOn(Category.tableName + " Category", "Product.category_id = Category.id")
				.where(p1)
				.orderBy("Brand.name", "Category.name")
				.limit(limit)
				.offset(offset); 
		
		List<StockItem> list = Dao.fetchAll(StockItem.class, q); 
		return list; 
	} 
	
	public static List<StockItem> fetchByCategoryLike(String strLike, int limit, int offset, String ...fieldNames){
		Predicate<String> p1 = new Predicate<>("Category.name", "LIKE", "%" + strLike + "%"); 
		//Specify custom query 
		Query q = new Query("select")
						.selectFields(fieldNames) 
						.setTable(StockItem.tableName + " Stock")
						.innerJoinOn(Product.tableName + " Product", "Stock.product_id = Product.id" )
						.innerJoinOn(Brand.tableName + " Brand", "Product.brand_id = Brand.id")
						.innerJoinOn(Category.tableName + " Category", "Product.category_id = Category.id")
						.where(p1)
						.orderBy("Brand.name", "Category.name")
						.limit(limit)
						.offset(offset); 
		List<StockItem> list = Dao.fetchAll(StockItem.class, q); 
		return list; 
	}	
	
	public static List<StockItem> fetchByPriceBetween(double min_price, double max_price, int limit, int offset, String ...fieldNames){
		Predicate<Double> p1 = new Predicate<>("Product.sell_price", ">=", min_price); 
		Predicate<Double> p2 = new Predicate<>("Product.sell_price", "<=", max_price); 
		
		Query q = new Query("select")
				.selectFields(fieldNames) 
				.setTable(StockItem.tableName + " Stock")
				.innerJoinOn(Product.tableName + " Product", "Stock.product_id = Product.id" )
				.innerJoinOn(Brand.tableName + " Brand", "Product.brand_id = Brand.id")
				.innerJoinOn(Category.tableName + " Category", "Product.category_id = Category.id")
				.where(p1)
				.andWhere(p2)
				.orderBy("Brand.name", "Category.name")
				.limit(limit)
				.offset(offset); 
		
		List<StockItem> list = Dao.fetchAll(StockItem.class, q); 
		return list; 
	}
	
	//Getters and setters 
	public int getProductId() {
		return product_id;
	}
	
	public void setId(int id){
		this.setHasId(true, id);
	}
	
	public void setProduct_id(int product_id) {
		super.setDirty(true, "product_id", product_id, Integer.class); 
		this.setId(product_id); 
		this.product_id = product_id;
		this.setProductId(product_id);
	}
	
	public int getBrand_id() {
		return brand_id;
	}

	public void setBrand_id(int brand_id) {
		this.brand_id = brand_id;
	}

	public int getCategory_id() {
		return category_id;
	}

	public void setCategory_id(int category_id) {
		this.category_id = category_id;
	}

	public String getBrand_name() {
		return brand_name;
	}

	public void setBrand_name(String brand_name) {
		this.brand_name = brand_name; 
		this.setBrandName(brand_name);
	}

	public String getCategory_name() {
		return category_name;
	}

	public void setCategory_name(String category_name) {
		this.category_name = category_name;
		this.setCategoryName(category_name);
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		super.setDirty(true, "quantity", quantity, Integer.class); 		
		this.quantity = quantity; 
		this.setProductQuantity(quantity);
	}
	
	public double getSellPrice(){
		return sell_price; 
	}
	
	public void setSell_price(double sellPrice){
		this.sell_price = sellPrice; 
		this.setSellPrice(sellPrice); 
	}
	
	//Properties 
	public void setProductId(int productId){
		this.productId.set(productId);
	}
	public IntegerProperty productIdProperty(){
		return productId; 
	}
    public void setBrandName(String brandName) {
        this.brandName.set(brandName); 
    }
	public StringProperty brandNameProperty() {
		return brandName;
	}
    public void setCategoryName(String categoryName) { 
        this.categoryName.set(categoryName); 
    } 
	public StringProperty categoryNameProperty() {
		return categoryName;
	}
	public void setProductQuantity(int productQuantity){
		this.productQuantity.set(productQuantity);
	} 
	public IntegerProperty productQuantityProperty(){
		return productQuantity; 
	}
	public void setSellPrice(double sellPrice){
		this.sellPrice.set(sellPrice); 
	}
	public DoubleProperty sellPriceProperty(){
		return sellPrice; 
	}
	
	
	@Override
	public String toString() {
		return "StockItem [product_id=" + product_id + ", brand_id=" + brand_id + ", category_id=" + category_id
				+ ", brand_name=" + brand_name + ", category_name=" + category_name + ", quantity=" + quantity
				+ ", brandName=" + brandName + ", categoryName=" + categoryName + ", productQuantity=" + productQuantity
				+ "]";
	}
	
}
