package application.controllers.admin;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import application.modal.ConfirmationModal;
import dao.Dao;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.Pagination;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.Pane;
import pojos.Person;
import query.Predicate;
import query.Query;

/**
 * Manage main client window 
 * @author kimgysen
 *
 */
public class ClientsController {
	
	@FXML private Pane pane_clientDetail; 
	private ClientShowController clientShowController; 
	private ClientEditController clientEditController; 
	private ClientOrdersController clientOrdersController; 
	
    @FXML private TableView<Person> clientsTable;
    @FXML private TableColumn<Person, String> colUserId;
    @FXML private TableColumn<Person, String> colFamilyName;
    @FXML private TableColumn<Person, String> colGivenName;
    @FXML private Pagination pagination; 
    
    //Static table info 
    private static int RESULTS_PER_PAGE = 16; 
    public static final int TABLE_STATE_ALL = 1; 
    public static final int TABLE_STATE_SEARCH_BY_NAME = 2; 
    public static final int TABLE_STATE_SEARCH_BY_ID = 3; 
    public static int TABLE_STATE = TABLE_STATE_ALL; 
    public static String SEARCH_STRING; 
    
    @FXML private ComboBox<String> cbb_client_search_by; 
    @FXML private TextField txt_client_search; 
    @FXML private Button btn_client_search; 
    @FXML private Label lbl_placeholder_clients; 
    
    @FXML private Button btn_new_client; 
    @FXML private Button btn_edit_client; 
    @FXML private Button btn_delete_client; 
    
    private Person activeClient; 
    private Parent viewShow; 
    private Parent viewEdit; 
    private Parent viewOrders; 
    private List<Parent> viewStackDetail = new ArrayList<>();  
    private static final int VIEW_SHOW = 1; 
    private static final int VIEW_EDIT = 2; 
    private static final int VIEW_ORDERS = 3; 
    private int viewState = VIEW_SHOW; 
    
    
    public void initialize(){ 
        //Add client combobox sort by change handler 
        cbb_client_search_by.getSelectionModel().selectedItemProperty().addListener((observableValue, oldValue, newValue) -> {
        	resetTable(); 
        	if(newValue.equals("id")){
        		txt_client_search.setPromptText("Enter client id"); 
        	} else if (newValue.equals("name")) {
        		txt_client_search.setPromptText("Enter client name"); 
        	}
        }); 
        
		//Set table column values 
		colUserId.setCellValueFactory(new PropertyValueFactory<Person, String>("id"));
		colFamilyName.setCellValueFactory(new PropertyValueFactory<Person, String>("familyName"));
		colGivenName.setCellValueFactory(new PropertyValueFactory<Person, String>("givenName"));
        
        //Add table change listener clients 
        clientsTable.getSelectionModel().selectedItemProperty().addListener((observableValue, oldValue, newValue) -> {
            if (clientsTable.getSelectionModel().getSelectedItem() != null) {
            	activeClient = newValue; 
            	if(viewState != VIEW_SHOW){ 
            		showView(viewShow); 
            		viewState = VIEW_SHOW; 
            	}
            	showClientDetails(); 
            }
        }); 
        
        pagination.currentPageIndexProperty().addListener((observableValue, oldValue, newValue) -> { 
        	showClients(RESULTS_PER_PAGE * (int) newValue); 
        }); 
        
        //Init 
        resetTable();
	}
	
    /**
     * Reset the table with all clients, without filter 
     */
	@FXML private void resetTable(){ 
		activeClient = null; 
		TABLE_STATE = TABLE_STATE_ALL; 
		setSearchText(""); 
		
		int count = Person.countAll(); 
		pagination.setPageCount(count / RESULTS_PER_PAGE + 1); 
		showClients(0); 
	}
	
	/**
	 * Manage search box key events 
	 * @param event
	 */
	@FXML public void onKeyReleasedSearchClient(KeyEvent event){ 
		if((event.getCode() == KeyCode.BACK_SPACE || event.getCode() == KeyCode.DELETE)){ 
			if(txt_client_search.getText().length() == 0){ 
				resetTable(); 
			}
		}else if (event.getCode() == KeyCode.ENTER) {
			searchClient(); 
		} 
	} 
    
	/**
	 * Public method to set searchbox text 
	 * @param strText
	 */
    public void setSearchText(String strText){
    	txt_client_search.setText(strText); 
    }
    
    /**
     * Manage detail pane view stack to avoid duplicate view instances when switching views 
     * @param view
     */
    private void showView(Parent view){  
    	if(viewStackDetail.size() > 0){ 
    		pane_clientDetail.getChildren().removeAll(viewStackDetail); 
    	}
		pane_clientDetail.getChildren().add(view); 
		viewStackDetail.add(view); 
    }
    
    /**
     * Load client detail 'show' view 
     * @param client
     */
	public void showClientDetails() { 
		if(viewShow == null){
	    	//Load "show" view as detail (right pane) 
			try { 
				FXMLLoader loader= new FXMLLoader(getClass().getResource("/application/resources/Client_show.fxml")); 
				viewShow = loader.load(); 
		    	clientShowController = (ClientShowController) loader.getController(); 
				
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
    	
		showView(viewShow); 
		viewState = VIEW_SHOW; 
	    if (activeClient != null) { 
	    	btn_delete_client.setVisible(true); 
	    	btn_edit_client.setVisible(true); 
	    } else {
	    	btn_edit_client.setVisible(false); 
	    	btn_delete_client.setVisible(false); 
	    }
	    clientShowController.setParentController(this);
	    clientShowController.setClient(activeClient); 
	}
	
	/**
	 * Load client detail 'edit' view 
	 */
	@FXML private void editClient(){ 
    	if(viewState != VIEW_EDIT){ 
    		if(viewEdit == null){
				FXMLLoader loader = new FXMLLoader(getClass().getResource("/application/resources/Client_edit.fxml")); 
				try {
					viewEdit = loader.load();
			    	clientEditController = (ClientEditController) loader.getController(); 
			    	clientEditController.setClientsController(this); 
				} catch (IOException e) {
					e.printStackTrace();
				}
    		}
    	} 
    	
    	showView(viewEdit); 
		viewState = VIEW_EDIT; 
		
		this.clientEditController.setClient(activeClient);
	} 
	
	/**
	 * Load client detail 'new' view -> we use the edit view for the same purpose 
	 */
	@FXML private void newClient(){ 
		activeClient = new Person(); 
		editClient(); 
	}
	
	/**
	 * Show client orders 
	 */
	public void showClientOrders(){ 
		if(viewState != VIEW_ORDERS){ 
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/application/resources/Client_orders.fxml")); 
			try {
				viewOrders = loader.load();
		    	clientOrdersController = (ClientOrdersController) loader.getController(); 
		    	clientOrdersController.setParentController(this); 
			} catch (IOException e) { 
				e.printStackTrace();
			}
			showView(viewOrders); 
			viewState = VIEW_ORDERS; 
			
			this.clientOrdersController.setClient(activeClient); 
		}
	}
	
	/**
	 * Delete client 
	 */
	@FXML private void deleteClient(){ 
    	boolean bDelete = new ConfirmationModal().show("Delete this item?", "Save client", "Yes", "No"); 
    	
    	if(bDelete){ 
			activeClient.delete(); 
			resetTable(); 
    	}
	}
	
	/**
	 * Validate input + execute search 
	 */
	public void searchClient(){
		String strSearch = txt_client_search.getText().trim(); 
		String strSearchBy = (String) cbb_client_search_by.getSelectionModel().getSelectedItem().toString(); 
		
		SEARCH_STRING = strSearch; 
		
		switch(strSearchBy){ 
		
			case "name": 
				TABLE_STATE = TABLE_STATE_SEARCH_BY_NAME; 
				pagination.setPageCount(countSearchByName(strSearch) / RESULTS_PER_PAGE + 1); 
				break; 
		
			case "id": 
				TABLE_STATE = TABLE_STATE_SEARCH_BY_ID; 
				break; 
		}
		showClients(0);
		selectTableRow(0); 
	}
	
	/**
	 * Show clients depending on state 
	 */
	public void showClients(int pageOffset){ 
		List<Person> list = null;  
		
		if(TABLE_STATE == TABLE_STATE_ALL){ 
			list = getUserList(pageOffset); 
			if(pageOffset == 0) pagination.setCurrentPageIndex(0); 
			
		} else if(TABLE_STATE == TABLE_STATE_SEARCH_BY_NAME){ 
			list = getUserListSearchByName(SEARCH_STRING, pageOffset); 
			if(list.size() == 0) lbl_placeholder_clients.setText("No results for '" + SEARCH_STRING + "'" ); 
			
		} else if(TABLE_STATE == TABLE_STATE_SEARCH_BY_ID){
			int id = SEARCH_STRING.equals("") ? 0 : Integer.parseInt(SEARCH_STRING); 
			list = getUserListSearchById(id); 
			pagination.setPageCount(1); 
			if(list.size() == 0) lbl_placeholder_clients.setText("No results for '" + SEARCH_STRING + "'" ); 
		}
		clientsTable.getItems().setAll(list); 
		
		//Select table row 
		int index = 0; 
		if(activeClient != null){ 
			int cnt = 0; 
			for (Person p : clientsTable.getItems()) {
            	if(activeClient.getId() == p.getId()){ 
            		index = cnt; 
            		break; 
            	}; 
            	cnt++; 
			}
		} 
		selectTableRow(index); 
	}
	
	/**
	 * Select a table row 
	 * @param index
	 */
	private void selectTableRow(int index){
		clientsTable.requestFocus();
		clientsTable.getSelectionModel().select(index); 
		clientsTable.getFocusModel().focus(index);
	}
	
	/**
	 * Fetch users 
	 * @return
	 */
    private List<Person> getUserList(int pageOffset){ 
		List<Person> list = Person.fetchAll(RESULTS_PER_PAGE, pageOffset); 
		return list; 
    }
    
    /**
     * Fetch users by id 
     * @param id
     * @return
     */
    private List<Person> getUserListSearchById(int id){ 
    	List<Person> list = new ArrayList<>(); 
    	Person p = new Person(); 
    	p.fetch(id); 
    	if(p.getId() != 0) list.add(p); 
    	return list; 
    }
    
    /**
     * Fetch users by name 
     * @param strSearch
     * @param pageOffset
     * @return
     */
    private List<Person> getUserListSearchByName(String strSearch, int pageOffset){
    	List<Person> list = Person.fetchLike(strSearch, RESULTS_PER_PAGE, pageOffset); 
    	return list; 
    }
    
    /**
     * Count all users by name for pagination 
     * @param strSearch
     * @return
     */
    private int countSearchByName(String strSearch){ 
		Predicate<String> p1 = new Predicate<>("given_name", "LIKE", "%" + strSearch + "%"); 
		Predicate<String> p2 = new Predicate<>("family_name", "LIKE", "%" + strSearch + "%"); 
		Predicate<String> p3 = new Predicate<>("CONCAT(given_name, ' ', family_name)", "LIKE", "%" + strSearch + "%"); //Concatenate to fullname
		Predicate<String> p4 = new Predicate<>("CONCAT(family_name, ' ', given_name)", "LIKE", "%" + strSearch + "%"); //Inverse
    	
    	Query q = new Query("select")
    					.selectFields("COUNT(*)")
    					.setTable("client")
						.where(p1)
						.orWhere(p2)
						.orWhere(p3)
						.orWhere(p4)
						.orderBy("family_name", "given_name"); 
    	
    	int count = 0;
		count = Dao.countAll(q);
    	return count; 
    }
    
}
