package dao;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.beanutils.BeanUtils;

import decorators.UpdatePredicatesList;
import factories.PojoFactory;
import query.Predicate;
import query.Query;

/**
 * Abstract class: common functionality for specific DAO pojo's 
 * @author kimg
 *
 * @param <T>
 */
public abstract class Dao<T> extends Model {
	private int id; 
	private boolean hasId = false; 
	private boolean dirty = false; 
	private List<Predicate<?>> updatePredicates = new UpdatePredicatesList(); 
	private List<String> fieldNames = new ArrayList<>(); 
	
	/**
	 * Constructor
	 */
	public Dao(String...fieldNames){ 
		for (int i = 0; i < fieldNames.length; i++) {
			this.fieldNames.add(fieldNames[i]); 
		}
	} 
	
//***********************************************************************************************	
//	Static fetch methods 
//***********************************************************************************************	
	
	/**
	 * Count all 
	 * @param q
	 * @return
	 */
	public static int countAll(Query q){ 
		int count = Model.preparedCountActions(q); 
		return count; 
	}
	
	/**
	 * Get a single pojo instance 
	 * @param classType
	 * @param tableName
	 * @param id
	 * @return
	 */
	public static <T extends Dao<T>> T fetch(Class<?> classType, String tableName, int id){ 
		return Dao.fetch(classType, tableName, id, null); 
	}
	
	/**
	 * Get a single pojo instance: overload method with optional PK definition 
	 * @param classType
	 * @param tableName
	 * @param id
	 * @return
	 */
	public static <T extends Dao<T>> T fetch(Class<?> classType, String tableName, int id, String primaryKey){ 
		String pk = primaryKey == null ? "id": primaryKey; 
		Predicate<Integer> p1 = new Predicate<>(pk, "=", id); 
		Query q = new Query("select")
						.selectFields()
						.setTable(tableName)
						.where(p1); 
		
		List<T> list = fetchAll(classType, q); 
		T obj = null; 
		if (list.size() > 0){
			obj = list.get(0); 
		}
		
		return obj; 
	}	
	
	/**
	 * Get list of pojos
	 * @param className, q
	 * @return List of pojos or null 
	 */
	public static <T extends Dao<T>> List<T> fetchAll(Class<?> classType, Query q){ 
		List<T> list = new ArrayList<>(); 
		
		try { 
			List<Map<String, Object>> results = null; 
			if(q.hasPredicates())
				results = Model.preparedGetActions(q); 
			else
				results = Model.simpleGetActions(q); 
			
			if(results != null){
				for (Map<String, Object> map : results) { 
					@SuppressWarnings("unchecked")
					T obj = (T) PojoFactory.createPojo(classType.getSimpleName()); 
					for (String key : map.keySet()) { 
						//http://commons.apache.org/proper/commons-beanutils/
						
						BeanUtils.setProperty(obj, key, map.get(key)); 
					} 
					obj.setDirty(false); 
					list.add(obj); 
				}
			}
			
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		} 
		
		return list; 
	}
	
//***********************************************************************************************	
//	If the object doesn't have an id set, Insert, otherwise Update if "dirty" 
//	Dirty logic: An object is marked as "dirty" when the setter is called upon an object that 
//				 is populated with an id (previously fetched or inserted). 
//	     		 If the object is not dirty, then nothing happens. 
//***********************************************************************************************	
	
	public void setHasId(boolean bHasId, int id){
		this.hasId = bHasId; 
		this.id = id; 
	}
	public boolean hasId(){
		return this.hasId; 
	}
	public int getId(){
		return this.id; 
	}
	
	public void setDirty(boolean bDirty){ 
		if(dirty == false && bDirty == true)
			dirty = true; 
		else if(bDirty == false)
			dirty = false; 
	}
	public void setDirty(boolean bDirty, String dbFieldName, Object dbFieldValue, Class<?> classType){
		if(!this.isDirty()){
			updatePredicates.clear(); 
		}
		this.setDirty(bDirty); 
		if(dbFieldValue != null)
			updatePredicates.add(new Predicate<>(dbFieldName, classType.cast(dbFieldValue))); 
	}
	public boolean isDirty(){
		return this.dirty; 
	}

	
//***********************************************************************************************	
//	"save" and "delete" actions are immediately executed on the object itself (non static)
//***********************************************************************************************	
	
	/**
	 * Save instance, "id" is assumed as primary key 
	 * @param tableName
	 * @return
	 */
	public int save(String tableName){
		return save(tableName, "id"); 
	}
	
	/**
	 * Save instance, provide custom primary key 
	 * @param tableName
	 * @param strPk
	 * @return
	 */
	public int save(String tableName, String strPk){
		Query q = null; 
		
		if(this.hasId()){
			Predicate<Integer> p1 = new Predicate<>(strPk, "=", this.getId()); 
			q = new Query("update")
					.setTable(tableName)
					.updateFields(updatePredicates)
					.where(p1); 
		} else {
			q = new Query("insert")
					.setTable(tableName)
					.updateFields(updatePredicates); 
		}
		
		int id = 0; 
		id = Model.preparedSetActions(q); 
		if(id > 0) this.setHasId(true, id);
		return id;		
	}
	
	/**
	 * Delete instance 
	 */
	public void delete(String tableName){
		Query q = null; 
		if(this.hasId()){
			Predicate<Integer> p1 = new Predicate<>("id", "=", this.getId()); 
			q = new Query("delete")
					.setTable(tableName)
					.where(p1); 
		}
		Model.preparedSetActions(q);
	}
	//Overload method to force delete 
	/**
	 * Delete instance 
	 * @param tableName
	 * @param hasId
	 */
	public void delete(String tableName, boolean hasId){
		this.setHasId(true, this.id);
		this.delete(tableName);
	}
	
		
}
