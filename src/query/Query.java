package query;

import java.util.ArrayList;
import java.util.List;

/**
 * Create a query string dynamically (experimental, not bug free) 
 * @author kimgysen
 *
 */
public class Query {
	
	private String type; 
	private String table; 
	
	private String queryString; 
	private List<Predicate<?>> predicates; 
	
	public Query(String type){ 
		this.predicates = new ArrayList<>(); 
		this.type = type; 
		this.createString(type); 
	} 
	
	public String getType() {
		if(type != null) 
			return type;
		else 
			return null; 
	}
	
	public String getTable() {
		if(table != null) 
			return table;
		else 
			return null; 
	}
	
	public List<Predicate<?>> getPredicates() {
		return predicates;
	}
	
	/**
	 * Set a raw query string 
	 * @param strQuery
	 * @return
	 */
	public Query setRawQuery(String strQuery){
		queryString = strQuery; 
		return this; 
	}
	
	public Query createString(String type){ 
		
		this.type = type.toUpperCase(); 
		
		switch(type.toUpperCase()){
		
		case "SELECT": 
			queryString = "SELECT"; 
			break; 
		
		case "INSERT": 
			queryString = "INSERT"; 
			break; 
		
		case "UPDATE": 
			queryString = "UPDATE"; 
			break; 
			
		case "DELETE": 
			queryString = "DELETE"; 
			break; 
		}
		
		return this; 
	}
	
	public Query setTable(String tableName){ 
		switch(this.getType()){
			case "SELECT": 
				queryString += " FROM " + tableName;
				break; 
			case "UPDATE": 
				queryString += " " + tableName;
				break; 
			case "INSERT": 
				queryString += " INTO " + tableName; 
				break; 
			case "DELETE": 
				queryString += " FROM " + tableName; 
				break; 
		}
		return this; 
	}
	
	public Query selectFields(String... fieldNames){ 
		
		String strFieldNames = " "; 
		if(fieldNames.length > 0){
			for (int i = 0; i < fieldNames.length; i++) {
				if(i > 0)
					strFieldNames += ", " + fieldNames[i]; 
				else
					strFieldNames += fieldNames[i]; 
			}
		} else {
			strFieldNames = " * "; 
		}
		
		queryString += strFieldNames; 
		
		return this; 
	}
	
	public Query innerJoinOn(String tableName, String on){
		String strJoin = " INNER JOIN " + tableName + " ON"; 
		strJoin += " " + on; 
		queryString += strJoin; 
		return this; 
	}
	
	public Query orderBy(String... fieldNames){ 
		String strOrderBy = " ORDER BY "; 
		for (int i = 0; i < fieldNames.length; i++) {
			if(i < fieldNames.length - 1)
				strOrderBy += fieldNames[i] + ", "; 
			else
				strOrderBy += fieldNames[i]; 
		}
		queryString += strOrderBy; 
		
		return this; 
	}
	
	public Query updateFields(List<Predicate<?>> predicates){
		
		switch(this.getType()){
			
			case "UPDATE": 
				queryString += " SET"; 
				
				int count = 1; 
				for (Predicate<?> p : predicates) {
					this.predicates.add(p); 
					queryString += " " + p.toString(); 
					if(count < predicates.size())
						queryString += ","; 
					count++; 
				}
				break; 
			
			case "INSERT": 
				queryString +="("; 
				//Field names 
				count = 1; 
				for (Predicate<?> p : predicates) {
					this.predicates.add(p); 
					queryString += " " + p.getOperand1(); 
					if(count < predicates.size())
						queryString += ","; 
					count++; 
				}
				//Values 
				queryString += ") VALUES("; 
				for (int i = 0; i < predicates.size(); i++) {
					if(i < predicates.size() - 1)
						queryString += " ?,"; 
					else 
						queryString += " ?)"; 
				}
				break; 
		}
		
		return this; 
	}
	
	public boolean hasPredicates(){
		return this.predicates.size() > 0; 
	}
	
	public Query where(Predicate<?> p){ 
		this.predicates.add(p); 
		queryString += " WHERE " + p.toString(); 
		return this; 
	}
	
	public Query orWhere(Predicate<?> p){
		this.predicates.add(p); 
		queryString += " OR " + p.toString(); 
		return this; 
	}
	
	public Query andWhere(Predicate<?> p){
		this.predicates.add(p); 
		queryString += " AND " + p.toString(); 
		return this; 
	}
	
	public Query limit(int limit){
		queryString += " LIMIT " + limit; 
		return this; 
	}
	
	public Query offset(int offset){
		queryString += " OFFSET " + offset; 
		return this; 
	}
	
	@Override
	public String toString() { 
		return queryString + ";"; 
	}
	
}
