package pojos;

import java.sql.SQLException;

import dao.Dao;
import dao.IDao;

public class Category extends Dao<Category> implements IDao<Category> {
	
	private int id; 
	private String name;
	
	public static String tableName = "category"; 
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	} 
	
	@Override
	public void fetch(int id) {
		Category c = Dao.fetch(Category.class, tableName, id); 
		if(c != null){ 
			this.setId(id); 
			this.setName(c.getName()); 
		}
	}
	@Override
	public void save() throws SQLException {
		
	}
	@Override
	public void delete() throws SQLException {
		
	}
	@Override
	public String toString() {
		return "Category [id=" + id + ", name=" + name + "]";
	} 
	
}
