package application.modal;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;

public class ConfirmationModalController {
	
	@FXML private Button btn_yes; 
	@FXML private Button btn_no; 
	@FXML private Label lbl_modal; 
	
	/**
	 * Show modal confirmation box  
	 * @param message
	 * @param title
	 * @param textYes
	 * @param textNo
	 * @return
	 */
	public void setModalText(String message, String textYes, String textNo){		
		lbl_modal.setText(message); 
		btn_yes.setText(textYes); 
		btn_no.setText(textNo); 
	 } 
	 
	public Button getBtnYes(){
		return btn_yes; 
	}
	public Button getBtnNo(){
		return btn_no; 
	}
	
}
