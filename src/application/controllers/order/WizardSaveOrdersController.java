package application.controllers.order;

import java.util.List;

import application.modal.ConfirmationModal;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import pojos.Order;

public class WizardSaveOrdersController {
	
	AddOrderController parentController; 
	
	@FXML private Label lbl_client_id; 
	@FXML private Label lbl_client_name; 
	@FXML private Label lbl_total_price; 
	
	@FXML private TableView<Order> ordersTable; 
	@FXML private TableColumn<Order, String> colProductString; 
	@FXML private TableColumn<Order, Integer> colQuantity; 
	@FXML private TableColumn<Order, Double> colPrice; 
	
	public void initialize(){
		
		//Set product table column values 
		colProductString.setCellValueFactory(new PropertyValueFactory<Order, String>("productIdString"));
		colQuantity.setCellValueFactory(new PropertyValueFactory<Order, Integer>("quantity"));
		colPrice.setCellValueFactory(new PropertyValueFactory<Order, Double>("orderValuePp"));
	}
	
	//Set parent controllers for back navigation 
	/**
	 * Set the parent controller for wizard navigation 
	 * @param parentController
	 */
	public void setParentController(AddOrderController parentController){
		this.parentController = parentController; 
		
		//Set client labels 
		lbl_client_id.setText(String.valueOf(this.parentController.getClient().getId())); 
		lbl_client_name.setText(this.parentController.getClient().getFamilyName() + " " + this.parentController.getClient().getGivenName()); 
		resetTable(); 
		lbl_total_price.setText(String.valueOf(calcTotalPrice()));
	}
	
	/**
	 * Go to place orders 
	 */
	@FXML public void gotoPlaceOrders(){
		this.parentController.gotoPlaceOrders();
	}
	
	/**
	 * Load orders into table 
	 */
	public void resetTable(){
		List<Order> list = this.parentController.getOrders(); 
		ordersTable.getItems().setAll(list); 
	}
	
	/**
	 * Calculate price of the total order
	 */
	private double calcTotalPrice(){
		double totalPrice = 0; 
for (Order order : this.parentController.getOrders()) {
		totalPrice += (order.getOrderValuePp() * order.getQuantity()); 
		}
		return totalPrice; 
	}
	
	/**
	 * Save all orders 
	 */
	@FXML public void saveOrders(){
		boolean bSave = new ConfirmationModal().show("Do you want to save?", "Save orders", "Yes", "No"); 
		if(bSave) this.parentController.saveOrders(); 
	}
	
}
