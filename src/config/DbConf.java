package config;

/**
 * Database configuration file 
 * @author kimg
 *
 */
public class DbConf { 
	
	//Set configuration here 
	static final String PORT="3306"; 
	static final String DB_NAME="clothes_shop"; 
	static final String HOST="localhost"; 
	static final String URL="jdbc:mysql://" + HOST + ":" + PORT + "/" + DB_NAME; 
	static final String USER="root"; 
	static final String PASSWORD=""; 
	
	//Convenience methods 
	public static String getUrl(){ 
		return DbConf.URL; 
	} 
	
	public static String getUser(){
		return DbConf.USER;
	}
	
	public static String getPassword(){
		return DbConf.PASSWORD; 
	} 
}
