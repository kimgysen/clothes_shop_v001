package dao;

import java.sql.SQLException;

/**
 * Get / save / delete a single instance from the db in RESTful fashion on the object
 * @author kimg
 *
 * @param <T>
 */
public interface IDao<T> {
	
	public void fetch(int id); 
	public void save() throws SQLException; 
	public void delete() throws SQLException; 

}
