package application.controllers.admin;

import java.util.List;

import dao.Dao;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Pagination;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import pojos.Order;
import pojos.Person;
import pojos.Product;
import query.Predicate;
import query.Query;

public class ClientOrdersController {
	
	@FXML Button btn_details; 
	@FXML Pagination pagination; 
	@FXML TableView<Order> ordersTable; 
	@FXML TableColumn<Order, Integer> colOrderId; 
	@FXML TableColumn<Order, String> colProductString; 
	@FXML TableColumn<Order, Integer> colQuantity; 
	@FXML TableColumn<Order, Double> colPrice; 
	@FXML Label lbl_total_value; 
	
	private static final int RESULTS_PER_PAGE = 14; 
	
	ClientsController parentController; 
	Person client; 
	
	public void initialize(){
		//Set placeholder 
		ordersTable.setPlaceholder(new Label("No orders for this fellah. Try Salma Hayek."));
		
		//Set table column values 
		colOrderId.setCellValueFactory(new PropertyValueFactory<Order, Integer>("id"));
		colProductString.setCellValueFactory(new PropertyValueFactory<Order, String>("productIdString"));
		colQuantity.setCellValueFactory(new PropertyValueFactory<Order, Integer>("quantity"));
		colPrice.setCellValueFactory(new PropertyValueFactory<Order, Double>("orderValuePp"));
		
		//Pagination event handler 
        pagination.currentPageIndexProperty().addListener((observableValue, oldValue, newValue) -> { 
        	showOrders(RESULTS_PER_PAGE * (int) newValue); 
        }); 
        
	}
	
	/**
	 * Get parent reference 
	 * @param parentController
	 */
	public void setParentController(ClientsController parentController){
		this.parentController = parentController; 
	}
	
	/**
	 * Set the client 
	 * @param client
	 */
	public void setClient(Person client){
		this.client = client; 
        showOrders(0); 
	}
	
	/**
	 * Navigate back to details 
	 */
	@FXML public void gotoDetails(){
		this.parentController.showClientDetails();
	}
	
    /**
     * Render table with all orders 
     */
	private void showOrders(int pageOffset){ 
		//get count + set pagination 
		int count = countOrdersByUser(client.getId()); 
		pagination.setPageCount(count / RESULTS_PER_PAGE + 1); 
		
		//Set table 
		List<Order> list = getOrderListByUser(client.getId(), pageOffset); 
		ordersTable.getItems().setAll(list); 
		
		//Set total value 
		double total_value = calcTotalValue(list); 
		lbl_total_value.setText(String.valueOf(total_value));
	}
	
	private double calcTotalValue(List<Order> orders){
		double subTotal = 0; 
		for (Order order : orders) {
			subTotal += order.getOrderValuePp() * order.getQuantity(); 
		}
		return subTotal; 
	}
	
    /**
     * Fetch orders by client id
     * @param Clientid
     * @return
     */
	
	private List<Order> getOrderListByUser(int clientId, int pageOffset){ 
    	Query q = new Query("select")
    				.selectFields()
    				.setTable(Order.tableName + " OrderBook")
    				.innerJoinOn(Person.tableName + " Client", "OrderBook.client_id = Client.id")
    				.where(new Predicate<Integer>("OrderBook.client_id", "=", (Integer) clientId))
    				.limit(RESULTS_PER_PAGE)
    				.offset(pageOffset); 
    	
    	List<Order> list = Dao.fetchAll(Order.class, q); 
    	
    	for (Order order : list) {
			//Set product Id String 
			Product product = new Product(); 
			product.fetch(order.getProduct_id());
			order.setProduct(product.getProductIdString()); 
			order.setOrder_value_pp(product.getSell_price());
		}
    	return list; 
		
	}
	
	/**
	 * Count orders by user for pagination 
	 * @param clientId
	 * @return
	 */
	private static int countOrdersByUser(int clientId){
    	Query q = new Query("select")
    			.selectFields("COUNT(*)")
				.setTable(Order.tableName + " OrderBook")
				.innerJoinOn(Person.tableName + " Client", "OrderBook.client_id = Client.id")
				.where(new Predicate<Integer>("OrderBook.client_id", "=", (Integer) clientId)); 
		
		return Dao.countAll(q); 

	}
}
