package application.controllers.admin;

import application.modal.ConfirmationModal;
import enums.Gender;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import pojos.Person;

/**
 * 'Edit' detail view 
 * We also use this class for creating a new client 
 * @author kimgysen
 *
 */

public class ClientEditController {
	
	private ClientsController clientsController; 
	
    @FXML private Label lbl_id;
    @FXML private TextField txt_given_name;
    @FXML private TextField txt_family_name;
    @FXML private TextField txt_email;
    @FXML private TextField txt_address;
    @FXML private TextField txt_zip;
    @FXML private TextField txt_city;
    @FXML private ToggleGroup genderGroup;
    @FXML private RadioButton opt_male; 
    @FXML private RadioButton opt_female; 
    @FXML private TextField txt_phone;
    
    @FXML private Button btn_cancel_edit; 
    @FXML private Button btn_save_client; 
    
    private Person client; 
    
    public void initialize(){
    }
    
    /**
     * Set client controller
     * @param clientsController
     */
    public void setClientsController(ClientsController clientsController){
    	this.clientsController = clientsController; 
    }
    
    /**
     * Set client 
     * @param client
     */
    public void setClient(Person client){
    	this.client = client; 
    	
	    if (client != null) {
	    	lbl_id.setText(Integer.toString(client.getId()));
	    	txt_given_name.setText(client.getGivenName());
	    	txt_family_name.setText(client.getFamilyName());
	    	if(client.getGender() == Gender.FEMALE.getGender()){
	    		opt_female.setSelected(true); 
	    	} else {
	    		opt_male.setSelected(true); 
	    	}
	    	txt_email.setText(client.getEmail());
	    	txt_address.setText(client.getAddress());
	    	txt_zip.setText(client.getPostal_code());
	    	txt_city.setText(client.getCity());
	    	txt_phone.setText(client.getPhone());
	        
	    } else { 
	    	lbl_id.setText("-");
	    	txt_given_name.setText("");
	    	txt_family_name.setText("");
	    	txt_email.setText("");
	    	txt_address.setText("");
	    	txt_zip.setText("");
	    	txt_city.setText("");
	    	txt_phone.setText("");
	    }    	
    	
    }
    
    /**
     * Read fields from the form 
     */
    public void readFields(){ 
    	if(client == null)
    		client = new Person(); 
    	
    	client.setGiven_name(txt_given_name.getText());
    	client.setFamily_name(txt_family_name.getText()); 
    	
    	int iGender = genderGroup.getSelectedToggle() == opt_male ? Gender.MALE.getGender() : Gender.FEMALE.getGender(); 
    	client.setGender(iGender); 
    	client.setEmail(txt_email.getText()); 
    	client.setAddress(txt_address.getText());
    	client.setPostal_code(txt_zip.getText()); 
    	client.setCity(txt_city.getText());
    	client.setPhone(txt_phone.getText());
    } 
    
    /**
     * Save the client 
     */
    @FXML public void saveClient(){ 
    	//Produce confirmation box first 
    	boolean bSave = new ConfirmationModal().show("Save this client?", "Save client", "Yes", "No"); 
    	if(bSave){
        	this.readFields(); 
			client.save(); 
			ClientsController.TABLE_STATE = ClientsController.TABLE_STATE_SEARCH_BY_ID; 
			ClientsController.SEARCH_STRING = String.valueOf(client.getId()); 
			clientsController.setSearchText(client.getFamilyName() + " " + client.getGivenName());
	    	clientsController.showClients(0); 
    	}
    }
    
    /**
     * Cancel the edit
     */
    @FXML void cancelEdit(){ 
    	clientsController.showClients(0); 
    }
    
}
