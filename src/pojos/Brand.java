package pojos;

import java.sql.SQLException;

import dao.Dao;
import dao.IDao;

public class Brand extends Dao<Brand> implements IDao<Brand> {
	
	private int id; 
	private String name; 
	
	public static String tableName = "brand"; 
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	@Override
	public void fetch(int id) {
		Brand b = Dao.fetch(Brand.class, tableName, id); 
		if(b != null){ 
			this.setId(id);
			this.setName(b.getName());
		}
	}
	
	@Override
	public void save() throws SQLException {
		
	}
	@Override
	public void delete() throws SQLException {
		
	}
	@Override
	public String toString() {
		return "Brand [id=" + id + ", name=" + name + "]";
	} 
	
}
