package application;
	
import java.io.IOException;
import application.controllers.MainController;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;


public class Main extends Application {
	@Override
	public void start(Stage primaryStage) {
		try {
			primaryStage.setTitle("Clothes shop");
			
			//https://gist.github.com/jewelsea/6460130
			primaryStage.setScene(
	                createScene(
	                    loadMainPane()
	                )
	            );
			
			primaryStage.show(); 
			
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
    /**
     * Loads the main fxml layout.
     * Sets up the vista switching VistaNavigator.
     * Loads the first vista into the fxml layout.
     *
     * @return the loaded pane.
     * @throws IOException if the pane could not be loaded.
     */
    private Pane loadMainPane() throws IOException {
        FXMLLoader loader = new FXMLLoader();
        
        Pane mainPane = (Pane) loader.load(
            getClass().getResourceAsStream(
                VistaNavigator.MAIN
            )
        );
        
        MainController mainController = loader.getController();
        
        VistaNavigator.setMainController(mainController);
        VistaNavigator.loadVista(VistaNavigator.DASHBOARD);

        return mainPane;
    }

    /**
     * Creates the main application scene.
     *
     * @param mainPane the main application layout.
     *
     * @return the created scene.
     */
    private Scene createScene(Pane mainPane) {
        Scene scene = new Scene(
            mainPane
        );

//        scene.getStylesheets().setAll(
//            getClass().getResource("vista.css").toExternalForm()
//        );

        return scene;
    }	
	
	public static void main(String[] args) { 
		
		launch(args);		
	}
}
