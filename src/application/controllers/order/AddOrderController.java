package application.controllers.order;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.layout.Pane;
import pojos.Order;
import pojos.Person;

/**
 * Launch and manage the wizard 
 * @author kimgysen
 *
 */
public class AddOrderController {
	
	@FXML Pane pane_addOrder_fragment; 
	
	@FXML private Button btn_selectClient; 
	@FXML private Button btn_placeOrders; 
	@FXML private Button btn_saveOrders; 
	
	private OrdersController parentController; 
	private WizardSelectClientController selectClientController; 
	private WizardPlaceOrdersController placeOrdersController; 
	private WizardSaveOrdersController saveOrdersController; 
	private WizardConfirmationController confirmationController; 
	
	private Parent viewSelectClient; 
	private Parent viewPlaceOrders; 
	private Parent viewSaveOrders; 
	private Parent viewConfirmation; 
	
	private List<Parent> viewStack = new ArrayList<>(); 
	private Person client; //Order: selected client 
	private List<Order> orders = Collections.synchronizedList(new ArrayList<Order>()); //Avoid concurrency issues
	
	public void initialize(){ 
		btn_placeOrders.setDisable(true);
		btn_saveOrders.setDisable(true);
		gotoSelectClient(); 
	}
	
	/**
	 * Set the current client 
	 * @param client
	 */
	public void setClient(Person client){
		this.client = client; 
		if(this.client == null){
			btn_placeOrders.setDisable(true);
			btn_saveOrders.setDisable(true);
		} else {
			btn_placeOrders.setDisable(false);
		}
	}
	
	/**
	 * Get the current client 
	 * @return
	 */
	public Person getClient(){
		return client; 
	}
	
	/**
	 * Give access to wizard panes to update the list 
	 * @return list of current orders 
	 */
	public List<Order> getOrders(){
		return orders; 
	}
	
	/**
	 * Add order to list 
	 */
	public void addOrder(Order order){
		this.orders.add(order); 
		placeOrdersController.showProducts(0); //Update the stock locally 
	}
	
	/**
	 * Delete order from list 
	 */
	public void deleteOrder(Order order){
		ArrayList<Order> toRemove = new ArrayList<>();
		for (Order itOrder : orders) {
			if(itOrder.equals(order)){
				toRemove.add(order); 
			}
		}
		this.orders.removeAll(toRemove); 
		placeOrdersController.showProducts(0); //Update the stock locally 
	} 
	
	/**
	 * Clear all orders 
	 */
	public void resetOrders(){
		this.orders.clear(); 
		placeOrdersController.showProducts(0); //Update the stock locally 
	}
	
	private void showView(Parent view){ 
	    /**
	     * Manage detail pane view stack to avoid duplicate view instances when switching views 
	     * @param view 
	     */
    	if(viewStack.size() > 0){ 
    		pane_addOrder_fragment.getChildren().removeAll(viewStack); 
    	}
		pane_addOrder_fragment.getChildren().add(view); 
		viewStack.add(view); 
	}
	
	/**
	 * Save all orders 
	 */
	public void saveOrders(){
		//This should be done with a db transaction, but I'm running out of time for the deadline. 
		//Since it's not indicated as a prerequisite for school, I omit it here... 
		for (Order order : orders) {
			order.save();
		}
		//Go to final confirmation screen if no db errors occur
		gotoConfirmation();
	}
	
	/**
	 * Set parent controller reference 
	 * @param parentController
	 */
	public void setParentController(OrdersController parentController){
		this.parentController = parentController; 
	}
	
	/**
	 * Navigate to order book
	 */
	@FXML public void gotoOrderBook(){
		this.parentController.showOrderBook();
	}
	
	/**
	 * Navigate to client selection pane 
	 */
	@FXML public void gotoSelectClient(){
		if(viewSelectClient == null){
			try { 
				FXMLLoader loader= new FXMLLoader(getClass().getResource("/application/resources/Order_select_client.fxml")); 
				viewSelectClient = loader.load(); 
				this.setClient(null);
				
				selectClientController = (WizardSelectClientController) loader.getController(); 
				selectClientController.setParentController(this); 
				
			} catch (IOException e) {
				e.printStackTrace();
			} 
		}
		showView(viewSelectClient); 
	}
	
	/**
	 * Navigate to place orders pane 
	 */
	@FXML public void gotoPlaceOrders(){ 
		if(viewPlaceOrders == null){
			try { 
				FXMLLoader loader= new FXMLLoader(getClass().getResource("/application/resources/Order_place_orders.fxml")); 
				viewPlaceOrders = loader.load(); 
				
				placeOrdersController = (WizardPlaceOrdersController) loader.getController(); 
				placeOrdersController.setParentController(this); 
				
			} catch (IOException e) {
				e.printStackTrace();
			} 
		}
		showView(viewPlaceOrders); 
	}
	
	/**
	 * Navigate to save orders pane 
	 */
	@FXML public void gotoSaveOrders(){ 
		try { 
			FXMLLoader loader= new FXMLLoader(getClass().getResource("/application/resources/Order_save_orders.fxml")); 
			viewSaveOrders = loader.load(); 
			
			saveOrdersController = (WizardSaveOrdersController) loader.getController(); 
			saveOrdersController.setParentController(this); 
			
		} catch (IOException e) {
			e.printStackTrace();
		} 
		showView(viewSaveOrders); 
	} 
	
	/**
	 * Navigate to confirmation pane 
	 */
	@FXML public void gotoConfirmation(){ 
		try { 
			FXMLLoader loader= new FXMLLoader(getClass().getResource("/application/resources/Order_save_confirmation.fxml")); 
			viewConfirmation = loader.load(); 
			
			confirmationController = (WizardConfirmationController) loader.getController(); 
			confirmationController.setParentController(this); 
			showView(viewConfirmation); 
			
		} catch (IOException e) {
			e.printStackTrace();
		} 
		
	} 
	
}
