package application.controllers;

import application.VistaNavigator;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;

public class DashboardController {
	
    @FXML private Button btn_admin;
    @FXML private Button btn_stock;
    @FXML private Button btn_orders;
    
    public void initialize(){
    	
    }
    
   @FXML void gotoAdmin(ActionEvent event) { 
       VistaNavigator.loadVista(VistaNavigator.ADMIN);
   }
   
   @FXML void gotoStock(ActionEvent event){ 
	   VistaNavigator.loadVista(VistaNavigator.STOCK);
   }
   
   @FXML void gotoOrderBook(ActionEvent event){ 
	   VistaNavigator.loadVista(VistaNavigator.ORDER_BOOK);
   }
   
}
