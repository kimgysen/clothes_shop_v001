package application.controllers.admin;

import application.modal.ConfirmationModal;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import pojos.Product;

/**
 * 'Edit' detail view 
 * We also use this class for creating a new client 
 * @author kimgysen
 *
 */

public class ProductEditController {
	
	private ProductsController productsController; 
	
    @FXML private Label lbl_id;
    
    @FXML private Button btn_cancel_edit; 
    @FXML private Button btn_save_client; 
    
    private Product product; 
    
    public void initialize(){
    }
    
    /**
     * Set products controller reference
     * @param productsController
     */
    public void setProductsController(ProductsController productsController){
    	this.productsController = productsController; 
    }
    
    /**
     * Set product 
     * @param product
     */
    public void setProduct(Product product){
    	this.product = product; 
    	
	    if (product != null) {
	    	lbl_id.setText(Integer.toString(product.getId()));
	        
	    } else { 
	    	lbl_id.setText("-");
	    }  
    	
    }
    
    /**
     * Read fields from the form 
     */
    public void readFields(){ 
    	if(product == null)
    		product = new Product(); 
    	
    } 
    
    /**
     * Save product
     */
    @FXML public void saveProduct(){ 
    	//Produce confirmation box first 
    	boolean bSave = new ConfirmationModal().show("Save this product?", "Save product", "Yes", "No"); 
    	if(bSave){
        	this.readFields(); 
			product.save(); 
			ProductsController.TABLE_STATE = ClientsController.TABLE_STATE_SEARCH_BY_ID; 
			ProductsController.SEARCH_STRING = String.valueOf(product.getId()); 
			productsController.setSearchText(""); 
	    	productsController.showProducts(0); 
    	}
    }
    
    @FXML void cancelEdit(){ 
    	productsController.showProducts(0); 
    }
    
}
