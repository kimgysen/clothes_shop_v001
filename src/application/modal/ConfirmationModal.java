package application.modal;

import java.io.IOException;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

public class ConfirmationModal {

	static private Stage stage; 
	static boolean btnYesClicked = false; 
	
	public boolean show(String message, String title, String textYes, String textNo){
		
		FXMLLoader loader= new FXMLLoader(getClass().getResource("./ConfirmationModal.fxml")); 
		Parent view = null; 
		try {
			view = loader.load();
		} catch (IOException e) { 
			e.printStackTrace();
		} 
		
    	ConfirmationModalController controller = (ConfirmationModalController) loader.getController(); 		
		controller.setModalText(message, textYes, textNo);
    	
		Button btnYes = controller.getBtnYes(); 
		Button btnNo = controller.getBtnNo(); 
		
		btnYes.setOnAction(e -> onClickBtnYes());
		btnNo.setOnAction(e -> onClickBtnNo());
		
		stage = new Stage(StageStyle.DECORATED); 
		stage.initModality(Modality.APPLICATION_MODAL);
		stage.setTitle(title);
		stage.setMinWidth(250); 
		
		Pane pane = new StackPane(); 
		pane.getChildren().addAll(view); 
		Scene scene = new Scene(pane); 
		
		stage.setScene(scene);
		stage.showAndWait(); 
		 
		return btnYesClicked; 		
	} 
	
	private static void onClickBtnYes(){
		 stage.close();
		 btnYesClicked = true;		
	}
	
	private static void onClickBtnNo(){
		 stage.close();
		 btnYesClicked = false;		
	}
}
