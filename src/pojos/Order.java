package pojos;

import java.util.List;

import dao.Dao;
import dao.IDao;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import query.Query;

public class Order extends Dao<Order> implements IDao<Order> {
	
	private int id; 
	private int product_id; 
	private int client_id; 
	private int quantity; 
	private double order_value_pp; 
	private String last_updated; 
	
	//Helper fields 
	private String client; 
	private String product; 
	
	//Table Property fields 
	private StringProperty clientIdString; 
	private StringProperty productIdString; 
	private DoubleProperty orderValuePp; 
	
	public static String tableName = "order_book"; 
	
	public Order(){
		this(0, 0, 0, 0); 
	}
	
	public Order(int order_id, int product_id, int client_id, int quantity){
		super("id", "product_id", "client_id", "quantity"); 
		this.id = order_id; 
		this.product_id = product_id; 
		this.client_id = client_id; 
		this.quantity = quantity; 
		
		this.clientIdString = new SimpleStringProperty(); 
		this.productIdString = new SimpleStringProperty(); 
		this.orderValuePp = new SimpleDoubleProperty(); 
	}
	
	public static int countAll(){
		Query q = new Query("select")
						.selectFields("COUNT(*)")
						.setTable(tableName); 
		
		int count = Dao.countAll(q); 
		
		return count; 
	} 
	
	/**
	 * Get a list of orders based on a query string
	 * @param fieldNames
	 * @return List of orders
	 */
	public static List<Order> fetchAll(int limit, int offset, String... fieldNames){ 
		
		//Specify custom query 
		Query q = new Query("select")
						.selectFields(fieldNames) 
						.setTable(tableName + " OrderBook")
						.orderBy("OrderBook.id")
						.limit(limit)
						.offset(offset); 
		
		List<Order> list = Dao.fetchAll(Order.class, q); 
		
		for (Order order : list) { 
			//Set client Id String 
			Person client = new Person(); 
			client.fetch(order.getClient_id()); 
			order.setClient(client.getClientIdString()); 
			
			//Set product Id String 
			Product product = new Product(); 
			product.fetch(order.getProduct_id());
			order.setProduct(product.getProductIdString());
		}
		
		return list; 
	} 	
	
	@Override
	public void fetch(int id) {
		Order o = Dao.fetch(Order.class, tableName, id); 
		//Set all properties on the current object 
		if(o != null){
			this.setId(id);
			this.setProduct_id(o.getProduct_id());
			this.setClient_id(o.getClient_id());
			this.setQuantity(o.getQuantity());
			
			Person p = new Person(); 
			p.fetch(o.getClient_id()); 
			if(p != null){
				this.setClientIdString(p.getId() + " - " + p.getFamilyName() + " " + p.getGivenName());
			}
			
			Product pt = new Product(); 
			pt.fetch(o.getProduct_id());
			if(pt != null){
				this.setProductIdString(pt.getId() + " - " + pt.getBrand_name() + " - " + pt.getCategory_name());
			}
		}
	}

	@Override
	public void save() {
		int id = super.save(tableName); 
		if( id > 0 ) this.id = id; 
		
		//The stock needs to be reduced when saving orders 
		//DB transaction omitted (lack of time) 
		StockItem s = new StockItem(); 
		s.fetch(this.getProduct_id()); 
		s.setQuantity(s.getQuantity() - this.quantity); 
		s.save(); 
	}
	
	@Override
	public void delete() {
		super.delete(tableName, true); 
		//The stock needs to be increased when saving orders 
		//DB transaction omitted (lack of time) 
		StockItem s = new StockItem(); 
		s.fetch(this.getProduct_id()); 
		s.setQuantity(s.getQuantity() + this.quantity); 
		s.save(); 
	}
	
	//Compare two orders 

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + product_id;
		result = prime * result + quantity;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Order other = (Order) obj;
		if (product_id != other.product_id)
			return false;
		if (quantity != other.quantity)
			return false;
		return true;
	}
	
	public int getId() {
		return id;
	}
	
	public void setId(int order_id) {
		this.setHasId(true, id); 
		this.id = order_id;
	}

	public int getProduct_id() {
		return product_id;
	}

	public void setProduct_id(int product_id) {
		super.setDirty(true, "product_id", product_id, Integer.class); 
		this.product_id = product_id;
	}

	public int getClient_id() {
		return client_id;
	}

	public void setClient_id(int client_id) {
		super.setDirty(true, "client_id", client_id, Integer.class); 
		this.client_id = client_id;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		super.setDirty(true, "quantity", quantity, Integer.class); 
		this.quantity = quantity;
	}
	
	public void setOrder_value_pp(double order_value_pp){
		this.order_value_pp = order_value_pp; 
		this.setOrderValuePp(order_value_pp);
	}
	
	public double getOrderValuePp(){
		return order_value_pp; 
	}
	
	public String getClient() {
		return client;
	}

	public void setClient(String client) {
		this.client = client;
		this.setClientIdString(client);
	}

	public String getProduct() {
		return product;
	}

	public void setProduct(String product) {
		this.product = product;
		this.setProductIdString(product);
	}
	
	public String getLast_updated(){
		return last_updated; 
	}
	
	public void setLast_updated(String last_updated){
		this.last_updated = last_updated; 
	}
	
	//Property 
    public void setClientIdString(String clientIdString) {
        this.clientIdString.set(clientIdString);
    }
	
	public StringProperty clientIdStringProperty() {
        return clientIdString;
    }
	
    public void setProductIdString(String productIdString) {
        this.productIdString.set(productIdString);
    }
	
	public StringProperty productIdStringProperty() {
        return productIdString;
    }
	
	public void setOrderValuePp(Double orderValuePp){
		this.orderValuePp.set(orderValuePp); 
	}
	
	public DoubleProperty orderValuePpProperty(){
		return orderValuePp; 
	}

	@Override
	public String toString() {
		return "Order [id=" + id + ", product_id=" + product_id + ", client_id=" + client_id + ", quantity=" + quantity
				+ ", order_value_pp=" + order_value_pp + ", last_updated=" + last_updated + ", client=" + client
				+ ", product=" + product + ", clientIdString=" + clientIdString + ", productIdString=" + productIdString
				+ ", orderValuePp=" + orderValuePp + "]";
	}
	
}
