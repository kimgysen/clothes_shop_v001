package application.controllers.order;

import java.util.ArrayList;
import java.util.List;

import application.modal.ConfirmationModal;
import dao.Dao;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.Pagination;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import pojos.Order;
import pojos.Person;
import pojos.Product;
import query.Predicate;
import query.Query;

public class OrderBookController {
	
    //Static table info 
    @FXML private TableView<Order> ordersTable;
    @FXML private TableColumn<Order, Integer> colOrderId;
    @FXML private TableColumn<Order, String> colClient;
    @FXML private TableColumn<Order, String> colProduct; 
    @FXML private TableColumn<Order, Integer> colQuantity; 
    @FXML private TableColumn<Order, String> colLastUpdated; 
    
    @FXML private ComboBox<String> cbb_order_search_by; 
    
    @FXML private TextField txt_order_search; 
    @FXML private Label lbl_placeholder_orders; 
    @FXML private Button btn_order_add; 
    @FXML private Button btn_order_delete; 
	@FXML Pagination pagination; 
	
	OrdersController parentController; 
	
    private static int RESULTS_PER_PAGE = 16; 
    public static final int TABLE_STATE_ALL = 1; 
    public static final int TABLE_STATE_SEARCH_BY_CLIENT_ID = 2; 
    public static final int TABLE_STATE_SEARCH_BY_CLIENT_NAME = 3; 
    public static final int TABLE_STATE_SEARCH_BY_ORDER_ID = 4; 
    public static int TABLE_STATE = TABLE_STATE_ALL; 
    public static String SEARCH_STRING; 
    
    private Order activeOrder; 
    
	public void initialize(){ 
		
		//Set table column values 
		colOrderId.setCellValueFactory(new PropertyValueFactory<Order, Integer>("id"));
		colClient.setCellValueFactory(new PropertyValueFactory<Order, String>("clientIdString"));
		colProduct.setCellValueFactory(new PropertyValueFactory<Order, String>("productIdString"));
		colQuantity.setCellValueFactory(new PropertyValueFactory<Order, Integer>("quantity"));
		colLastUpdated.setCellValueFactory(new PropertyValueFactory<Order, String>("last_updated"));
		
		resetTable(); 
		
        //Add client combobox sort by change handler 
        cbb_order_search_by.getSelectionModel().selectedItemProperty().addListener((observableValue, oldValue, newValue) -> {
        	if(newValue.equals("order id")){
        		txt_order_search.setPromptText("Enter order id"); 
        	} else if (newValue.equals("client name")) {
        		txt_order_search.setPromptText("Enter client name"); 
        	} else if(newValue.equals("client id")){
        		txt_order_search.setPromptText("Enter client id");
        	}
        	resetTable(); 
        }); 
		
        //Add table change listener clients 
        ordersTable.getSelectionModel().selectedItemProperty().addListener((observableValue, oldValue, newValue) -> {
            if (ordersTable.getSelectionModel().getSelectedItem() != null) {
    	    	btn_order_delete.setVisible(true); 
    	    	activeOrder = newValue; 
            }
        }); 
		
        pagination.currentPageIndexProperty().addListener((observableValue, oldValue, newValue) -> { 
        	showOrders(RESULTS_PER_PAGE * (int) newValue); 
        }); 
	}
	
	public void setParentController(OrdersController parentController){
		this.parentController = parentController; 
	}
	
	@FXML public void addOrder(){
		this.parentController.showAddOrder(); 
	}
	
	/**
	 * Delete client 
	 */
	@FXML private void deleteOrder(){ 
    	boolean bDelete = new ConfirmationModal().show("Delete this item?", "Save client", "Yes", "No"); 
    	
    	if(bDelete){ 
			activeOrder.delete(); 
			resetTable(); 
    	}
	}
	
    /**
     * Reset the table with all orders, without filter 
     */
	@FXML private void resetTable(){ 
		TABLE_STATE = TABLE_STATE_ALL; 
		setSearchText(""); 
		
		int count = Order.countAll(); 
		pagination.setPageCount(count / RESULTS_PER_PAGE + 1); 
		showOrders(0); 
	}
	
	/**
	 * Manage search box key events 
	 * @param event
	 */
	@FXML public void onKeyReleasedSearchOrder(KeyEvent event){ 
		if((event.getCode() == KeyCode.BACK_SPACE || event.getCode() == KeyCode.DELETE)){ 
			if(txt_order_search.getText().length() == 0){ 
				resetTable(); 
			}
		}else if (event.getCode() == KeyCode.ENTER) {
			searchOrder(); 
		} 
	} 
    
	/**
	 * Public method to set searchbox text 
	 * @param strText
	 */
    public void setSearchText(String strText){
    	txt_order_search.setText(strText); 
    }
    
	/**
	 * Validate input + execute search 
	 */
	public void searchOrder(){
		String strSearch = txt_order_search.getText().trim(); 
		String strSearchBy = ((String) cbb_order_search_by.getSelectionModel().getSelectedItem().toString()).trim(); 
		
		SEARCH_STRING = strSearch; 
		switch(strSearchBy){ 
			
			case "order id": 
				TABLE_STATE = TABLE_STATE_SEARCH_BY_ORDER_ID; 
				break; 
			
			case "client name": 
				TABLE_STATE = TABLE_STATE_SEARCH_BY_CLIENT_NAME; 
				break; 
			
			case "client id": 
				TABLE_STATE = TABLE_STATE_SEARCH_BY_CLIENT_ID; 
				break; 
		}
		showOrders(0);
	}
	
	/**
	 * Show orders depending on state 
	 */
	public void showOrders(int pageOffset){ 
		List<Order> list = new ArrayList<>();  
		if(TABLE_STATE == TABLE_STATE_ALL){ 
			list = getOrderList(pageOffset); 
			if(pageOffset == 0) pagination.setCurrentPageIndex(0); 
			
		} else if(TABLE_STATE == TABLE_STATE_SEARCH_BY_ORDER_ID){ 
			pagination.setPageCount(1); 
			if(utils.TypeUtils.isNumeric(SEARCH_STRING)){
				int orderId = SEARCH_STRING.equals("") ? 0 : Integer.parseInt(SEARCH_STRING); 
				list = getOrderListSearchById(orderId); 
			}
			if(list.size() == 0) lbl_placeholder_orders.setText("No results for '" + SEARCH_STRING + "'" ); 
			
		} else if(TABLE_STATE == TABLE_STATE_SEARCH_BY_CLIENT_ID){
			pagination.setPageCount(1); 
			if(utils.TypeUtils.isNumeric(SEARCH_STRING)){
				int clientId = SEARCH_STRING.equals("") ? 0 : Integer.parseInt(SEARCH_STRING); 
				list = getOrderListSearchByClientId(clientId, pageOffset); 
			}
			if(list.size() == 0) lbl_placeholder_orders.setText("No results for '" + SEARCH_STRING + "'" ); 
			
		} else if(TABLE_STATE == TABLE_STATE_SEARCH_BY_CLIENT_NAME){
			pagination.setPageCount(countSearchByClientName(SEARCH_STRING) / RESULTS_PER_PAGE + 1); 
			list = getOrderListSearchByClientName(SEARCH_STRING); 
		}
		ordersTable.getItems().setAll(list); 
		
	}
	
	/**
	 * Fetch orders 
	 * @return
	 */
    private List<Order> getOrderList(int pageOffset){ 
		List<Order> list = Order.fetchAll(RESULTS_PER_PAGE, pageOffset); 
		return list; 
    }	
    
    /**
     * Fetch orders by id 
     * @param id
     * @return
     */
    private List<Order> getOrderListSearchById(int id){ 
    	List<Order> list = new ArrayList<>(); 
    	Order o = new Order(); 
    	o.fetch(id); 
    	if(o.getId() != 0) list.add(o); 
    	return list; 
    }
    
    /**
     * Fetch orders by client id
     * @param Clientid
     * @return
     */
    private List<Order> getOrderListSearchByClientId(int clientId, int pageOffset){
    	Query q = new Query("select")
    				.selectFields("OrderBook.*")
    				.setTable(Order.tableName + " OrderBook")
    				.innerJoinOn(Person.tableName + " Client", "OrderBook.client_id = Client.id")
    				.where(new Predicate<Integer>("OrderBook.client_id", "=", (Integer) clientId))
    				.limit(RESULTS_PER_PAGE)
    				.offset(pageOffset); 
    	
    	List<Order> list = Dao.fetchAll(Order.class, q); 
    	for (Order order : list) {
			//Set client Id String 
			Person client = new Person(); 
			client.fetch(order.getClient_id()); 
			order.setClient(client.getClientIdString()); 
			
			//Set product Id String 
			Product product = new Product(); 
			product.fetch(order.getProduct_id());
			order.setProduct(product.getProductIdString());
		}
    	return list; 
    }
    
    private List<Order> getOrderListSearchByClientName(String strSearch){
		Predicate<String> p1 = new Predicate<>("Client.given_name", "LIKE", "%" + strSearch + "%"); 
		Predicate<String> p2 = new Predicate<>("Client.family_name", "LIKE", "%" + strSearch + "%"); 
		Predicate<String> p3 = new Predicate<>("CONCAT(Client.given_name, ' ', Client.family_name)", "LIKE", "%" + strSearch + "%"); //Concatenate to fullname
		Predicate<String> p4 = new Predicate<>("CONCAT(Client.family_name, ' ', Client.given_name)", "LIKE", "%" + strSearch + "%"); //Inverse
    	
    	Query q = new Query("select")
    					.selectFields()
    					.setTable(Order.tableName + " OrderBook")
    					.innerJoinOn(Person.tableName + " Client", "OrderBook.client_id = Client.id")
						.where(p1)
						.orWhere(p2)
						.orWhere(p3)
						.orWhere(p4)
						.orderBy("Client.family_name", "Client.given_name"); 
    	
    	List<Order> list = Dao.fetchAll(Order.class, q);
    	for (Order order : list) {
			//Set client Id String 
			Person client = new Person(); 
			client.fetch(order.getClient_id()); 
			order.setClient(client.getClientIdString()); 
			
			//Set product Id String 
			Product product = new Product(); 
			product.fetch(order.getProduct_id());
			order.setProduct(product.getProductIdString());
		}
    	return list; 
    }
    
    /**
     * Count all users by client name for pagination 
     * @param strSearch
     * @return
     */
    private int countSearchByClientName(String strSearch){ 
		Predicate<String> p1 = new Predicate<>("Client.given_name", "LIKE", "%" + strSearch + "%"); 
		Predicate<String> p2 = new Predicate<>("Client.family_name", "LIKE", "%" + strSearch + "%"); 
		Predicate<String> p3 = new Predicate<>("CONCAT(Client.given_name, ' ', Client.family_name)", "LIKE", "%" + strSearch + "%"); //Concatenate to fullname
		Predicate<String> p4 = new Predicate<>("CONCAT(Client.family_name, ' ', Client.given_name)", "LIKE", "%" + strSearch + "%"); //Inverse
    	
    	Query q = new Query("select")
    					.selectFields("COUNT(*)")
    					.setTable(Order.tableName + " OrderBook")
    					.innerJoinOn(Person.tableName + " Client", "OrderBook.client_id = Client.id")
						.where(p1)
						.orWhere(p2)
						.orWhere(p3)
						.orWhere(p4); 
    	
    	int count = 0;
		count = Dao.countAll(q); 
    	return count; 
    }    
    
}
