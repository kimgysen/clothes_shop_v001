package enums;

public enum Gender {
	MALE(1), FEMALE(2); 
	
	private int gender; 
	
	Gender(int gender){
		this.gender = gender; 
	}
	
	public int getGender(){
		return this.gender; 
	}
	
	public static String getGenderAsString(int gender){ 
		String strGender = null; 
		
		switch (gender){ 
			case 1: 
				strGender = "Male"; 
				break; 
				
			case 2: 
				strGender = "Female"; 
				break; 
		}
		
		return strGender;
	} 
	
	public static int getGenderAsInteger(String strGender){
		int iGender = 0; 
		
		switch (strGender.toUpperCase()){ 
			case "M": 
				iGender = 1; 
				break; 
				
			case "V": 
			case "F": 
				iGender = 2; 
				break; 
		}
		
		return iGender; 
	}
}
