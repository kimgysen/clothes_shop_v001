package application.controllers.admin;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import application.modal.ConfirmationModal;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.Pagination;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.Pane;
import pojos.Product;

public class ProductsController {
	
	@FXML private Pane pane_productDetail; 
	private ProductShowController productShowController; 
	//private ProductEditController productEditController; 
	
    @FXML private TableView<Product> productsTable;
    @FXML private TableColumn<Product, String> colProductId;
    @FXML private TableColumn<Product, String> colBrand;
    @FXML private TableColumn<Product, String> colCategory;
    @FXML private TableColumn<Product, Double> colSellPrice;
    
    @FXML private Pagination pagination; 
    
    //Static table info 
    private static int RESULTS_PER_PAGE = 16; 
    public static final int TABLE_STATE_ALL = 1; 
    public static final int TABLE_STATE_SEARCH_BY_ID = 2; 
    public static final int TABLE_STATE_SEARCH_BY_BRAND = 3; 
    public static final int TABLE_STATE_SEARCH_BY_CATEGORY = 4; 
    public static int TABLE_STATE = TABLE_STATE_ALL; 
    public static String SEARCH_STRING; 
    
    @FXML private ComboBox<String> cbb_product_search_by; 
    @FXML private TextField txt_product_search; 
    @FXML private Button btn_product_search; 
    @FXML private Label lbl_placeholder_products; 
    
    @FXML private Button btn_new_product; 
    @FXML private Button btn_edit_product; 
    @FXML private Button btn_delete_product; 
    
    private Product activeProduct; 
    private Parent viewShow; 
    private Parent viewEdit; 
    private List<Parent> viewStackDetail = new ArrayList<>();  
    private static final int VIEW_SHOW = 1; 
    private static final int VIEW_EDIT = 2; 
    private int viewState = VIEW_SHOW; 
        
    public void initialize(){
    	//Load "show" view as detail (right pane) 
		try { 
			FXMLLoader loader= new FXMLLoader(getClass().getResource("/application/resources/Product_show.fxml")); 
			viewShow = loader.load(); 
			showView(viewShow); 
			
	    	productShowController = (ProductShowController) loader.getController(); 
			
		} catch (IOException e) {
			e.printStackTrace();
		}
    	
		//Set table column values 
		colProductId.setCellValueFactory(new PropertyValueFactory<Product, String>("id"));
		colBrand.setCellValueFactory(new PropertyValueFactory<Product, String>("brandName"));
		colCategory.setCellValueFactory(new PropertyValueFactory<Product, String>("categoryName"));        
		colSellPrice.setCellValueFactory(new PropertyValueFactory<Product, Double>("sellPrice"));        
        
        //Add table change listener clients 
        productsTable.getSelectionModel().selectedItemProperty().addListener((observableValue, oldValue, newValue) -> {
            if (productsTable.getSelectionModel().getSelectedItem() != null) {
            	activeProduct = newValue; 
            	if(viewState != VIEW_SHOW){ 
            		showView(viewShow); 
            		viewState = VIEW_SHOW; 
            	} 
            	showProductDetails(newValue); 
            }
        }); 
		
        pagination.currentPageIndexProperty().addListener((observableValue, oldValue, newValue) -> { 
        	showProducts(RESULTS_PER_PAGE * (int) newValue); 
        }); 
        
        //Add product combobox sort by change handler 
        cbb_product_search_by.getSelectionModel().selectedItemProperty().addListener((observableValue, oldValue, newValue) -> {
        	resetTable(); 
        	if(newValue.equals("id")){
        		txt_product_search.setPromptText("Enter product id"); 
        	} else if (newValue.equals("name")) {
        		txt_product_search.setPromptText("Enter brand"); 
        	} else if (newValue.equals("category")){
        		txt_product_search.setPromptText("Enter category"); 
        	}
        }); 
        
        //Init 
        resetTable(); 
        
        //Functionality not implemented yet
        btn_delete_product.setDisable(true);
        btn_new_product.setDisable(true);
        btn_edit_product.setDisable(true);
    }
	
    /**
     * Reset the table with all products, without filter 
     */
	@FXML private void resetTable(){ 
		activeProduct = null; 
		TABLE_STATE = TABLE_STATE_ALL; 
		setSearchText(""); 
		
		int count = Product.countAll(); 
		pagination.setPageCount(count / RESULTS_PER_PAGE + 1); 
		showProducts(0); 
	}    
    
	/**
	 * Manage search box key events 
	 * @param event
	 */
	@FXML public void onKeyReleasedSearchProduct(KeyEvent event){ 
		if((event.getCode() == KeyCode.BACK_SPACE || event.getCode() == KeyCode.DELETE)){ 
			if(txt_product_search.getText().length() == 0){ 
				resetTable(); 
			}
		}else if (event.getCode() == KeyCode.ENTER) {
			searchProduct(); 
		}
	} 
	
    /**
     * Manage detail pane view stack to avoid duplicate view instances when switching views 
     * @param view
     */
    private void showView(Parent view){  
    	if(viewStackDetail.size() > 0){ 
    		pane_productDetail.getChildren().removeAll(viewStackDetail); 
    	}
		pane_productDetail.getChildren().add(view); 
		viewStackDetail.add(view); 
    }
    
    /**
     * Load client detail 'show' view 
     * @param client
     */
	private void showProductDetails(Product product) {
	    if (product != null) { 
	    	btn_delete_product.setVisible(true); 
	    	btn_edit_product.setVisible(true); 
	    } else {
	    	btn_edit_product.setVisible(false); 
	    	btn_delete_product.setVisible(false); 
	    }
	    productShowController.setProduct(product); 
	}
	
	/**
	 * Public method to set searchbox text 
	 * @param strText
	 */
    public void setSearchText(String strText){
    	txt_product_search.setText(strText); 
    }
    
	/**
	 * Validate input + execute search 
	 */
	public void searchProduct(){ 
		String strSearch = txt_product_search.getText().trim(); 
		String strSearchBy = (String) cbb_product_search_by.getSelectionModel().getSelectedItem().toString(); 
		
		SEARCH_STRING = strSearch; 
		
		switch(strSearchBy){ 
		
			case "id": 
				TABLE_STATE = TABLE_STATE_SEARCH_BY_ID; 
				//pagination.setPageCount(countSearchByBrand(strSearch) / RESULTS_PER_PAGE + 1); 
				break; 
		
			case "brand": 
				TABLE_STATE = TABLE_STATE_SEARCH_BY_BRAND; 
				break; 
			
			case "category": 
				TABLE_STATE = TABLE_STATE_SEARCH_BY_CATEGORY; 
				break; 
		}
		showProducts(0);
		selectTableRow(0); 
	}	
	
	/**
	 * Load product detail 'edit' view 
	 */
	@FXML private void editProduct(){ 
    	if(viewState != VIEW_EDIT){ 
    		if(viewEdit == null){
				FXMLLoader loader = new FXMLLoader(getClass().getResource("/application/resources/Product_edit.fxml")); 
				try {
					viewEdit = loader.load();
			    	//productEditController = (ProductEditController) loader.getController(); 
			    	//productEditController.setProductsController(this); 
				} catch (IOException e) {
					e.printStackTrace();
				}
    		}
    	} 
    	
    	//showView(viewEdit); 
		viewState = VIEW_EDIT; 
		
		//this.productEditController.setProduct(activeProduct);
	} 
	
	/**
	 * Load product detail 'new' view -> we use the edit view for the same purpose 
	 */
	@FXML private void newProduct(){ 
		activeProduct = new Product(); 
		//editProduct(); 
	}	
	
	/**
	 * Delete product 
	 */
	@FXML private void deleteProduct(){ 
    	boolean bDelete = new ConfirmationModal().show("Delete this item?", "Save product", "Yes", "No"); 
    	
    	if(bDelete){ 
    		//To do
    	}
	}
	
	/**
	 * Show products depending on state 
	 */
	public void showProducts(int pageOffset){ 
		List<Product> list = null;  
		
		if(TABLE_STATE == TABLE_STATE_ALL){ 
			list = getProductList(pageOffset); 
			if(pageOffset == 0) pagination.setCurrentPageIndex(0); 
			
		} else if(TABLE_STATE == TABLE_STATE_SEARCH_BY_ID){ 
			int id = SEARCH_STRING.equals("") ? 0 : Integer.parseInt(SEARCH_STRING); 
			list = getProductListSearchById(id); 
			pagination.setPageCount(1); 
			if(list.size() == 0) lbl_placeholder_products.setText("No results for '" + SEARCH_STRING + "'" ); 
			
		} else if(TABLE_STATE == TABLE_STATE_SEARCH_BY_BRAND){ 
			list = getProductListSearchByBrandName(SEARCH_STRING, pageOffset); 
			if(list.size() == 0) lbl_placeholder_products.setText("No results for '" + SEARCH_STRING + "'" ); 
			
		} else if(TABLE_STATE == TABLE_STATE_SEARCH_BY_CATEGORY){
			list = getProductListSearchByCategoryName(SEARCH_STRING, pageOffset); 
			if(list.size() == 0) lbl_placeholder_products.setText("No results for '" + SEARCH_STRING + "'" ); 
		}
		productsTable.getItems().setAll(list); 
		
		//Select table row 
		int index = 0; 
		if(activeProduct != null){ 
			int cnt = 0; 
			for (Product p : productsTable.getItems()){ 
            	if(activeProduct.getId() == p.getId()){ 
            		index = cnt; 
            		break; 
            	}; 
            	cnt++; 
			}
		} 
		selectTableRow(index); 
	}
	private void selectTableRow(int index){
		productsTable.requestFocus();
		productsTable.getSelectionModel().select(index); 
		productsTable.getFocusModel().focus(index);
	} 
	
	/**
	 * Fetch products 
	 * @return
	 */
    private List<Product> getProductList(int pageOffset){ 
		List<Product> list = Product.fetchAll(RESULTS_PER_PAGE, pageOffset, "Product.*, Brand.name as brand_name, Category.name as category_name"); 
		return list; 
    }
    
    /**
     * Fetch products by id 
     * @param id
     * @return
     */
    private List<Product> getProductListSearchById(int id){ 
    	List<Product> list = new ArrayList<>(); 
    	Product p = new Product(); 
    	p.fetch(id); 
    	if(p.getId() != 0) list.add(p); 
    	return list; 
    } 
    
    /**
     * Fetch products by brand name 
     * @param strSearch
     * @param pageOffset
     * @return
     */
    private List<Product> getProductListSearchByBrandName(String strSearch, int pageOffset){
    	List<Product> list = Product.fetchByBrandLike(strSearch, RESULTS_PER_PAGE, pageOffset, "Product.*", "Brand.name as brand_name", "Category.name as category_name"); 
    	return list; 
    }
    
    /**
     * Fetch products by brand name 
     * @param strSearch
     * @param pageOffset
     * @return
     */
    private List<Product> getProductListSearchByCategoryName(String strSearch, int pageOffset){
    	List<Product> list = Product.fetchByCategoryLike(strSearch, RESULTS_PER_PAGE, pageOffset, "Product.*", "Brand.name as brand_name", "Category.name as category_name"); 
    	return list; 
    }
    
}
