package application.controllers.order;

import javafx.fxml.FXML;

public class WizardConfirmationController {
	
	AddOrderController parentController; 
	
	/**
	 * Set parent controller reference 
	 * @param parentController
	 */
	public void setParentController(AddOrderController parentController){
		this.parentController = parentController; 
	}
	
	/**
	 * Navigate to the order book (overview)
	 */
	@FXML public void gotoOverview(){
		this.parentController.gotoOrderBook();
	}
	
	
}
