package application.controllers.admin;

import java.io.IOException;

import application.VistaNavigator;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Button;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.layout.Pane;

/**
 * Control the flow of the Admin panel 
 * Manages tab change Clients / Products  
 * @author kimgysen
 *
 */
public class AdminController {
		
	@FXML private Button btn_dashboard;
    @FXML private TabPane tabpane_admin; 
    
    @FXML private Tab tab_clients; 
    @FXML private Tab tab_products; 
    @FXML private Tab tab_brands; 
    @FXML private Tab tab_categories; 
    
    @FXML private Pane pane_clients; 
    @FXML private Pane pane_products; 
    @FXML private Pane pane_brands; 
    @FXML private Pane pane_categories; 
    
    boolean pane_products_instantiated; 
    boolean pane_brands_instantiated; 
    boolean pane_categories_instantiated; 
    
    public void initialize(){
    	
		try {
			pane_clients.getChildren().add(FXMLLoader.load(getClass().getResource("/application/resources/Clients.fxml")));
		} catch (IOException e1) {
			e1.printStackTrace();
		}
    	
        //Add tabpane click handlers 
        tabpane_admin.getSelectionModel().selectedItemProperty().addListener((observableValue, oldValue, newValue) -> {
            if(newValue == tab_clients) {
            	//Instantiated on init 
            } else if(newValue == tab_products){ 
            	//Optimize for performance 
            	try { 
            		if(!pane_products_instantiated){
    					pane_products.getChildren().add(FXMLLoader.load(getClass().getResource("/application/resources/Products.fxml")));
            			pane_products_instantiated = true; 
            		}
				} catch (Exception e) {
					e.printStackTrace();
				}
            }else if(newValue == tab_brands){
               	//Optimize for performance 
            	try { 
            		if(!pane_brands_instantiated){
    					pane_brands.getChildren().add(FXMLLoader.load(getClass().getResource("/application/resources/Brands.fxml")));
            			pane_brands_instantiated = true; 
            		}
				} catch (Exception e) {
					e.printStackTrace();
				}
            }else if(newValue == tab_categories){
               	//Optimize for performance 
            	try { 
            		if(!pane_categories_instantiated){
    					pane_categories.getChildren().add(FXMLLoader.load(getClass().getResource("/application/resources/Categories.fxml")));
            			pane_categories_instantiated = true; 
            		}
				} catch (Exception e) {
					e.printStackTrace();
				}
            }
        }); 
        
	}
	
	@FXML
	void gotoDashboard(ActionEvent event) {
       VistaNavigator.loadVista(VistaNavigator.DASHBOARD);
	}
	
}
