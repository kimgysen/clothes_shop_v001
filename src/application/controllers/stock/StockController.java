package application.controllers.stock;

import java.util.ArrayList;
import java.util.List;

import application.VistaNavigator;
import application.modal.ConfirmationModal;
import dao.Dao;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.Pagination;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.HBox;
import pojos.Brand;
import pojos.Category;
import pojos.Product;
import pojos.StockItem;
import query.Predicate;
import query.Query;

/**
 * Manage stock window 
 * @author kimg
 *
 */
public class StockController {
	
    @FXML private ComboBox<String> cbb_stock_search_by; 
    @FXML private TextField txt_stock_search; 
    @FXML private Button btn_stock_search; 
    @FXML private Label lbl_placeholder_stock; 
    
    @FXML private HBox hbox_price_details; 
    @FXML private TextField txt_min_price; 
    @FXML private TextField txt_max_price; 
    
    @FXML private Button btn_stock_new; 
    @FXML private Button btn_stock_delete; 
    
    @FXML private TableView<StockItem> stockTable;
    @FXML private TableColumn<StockItem, Integer> colProductId;
    @FXML private TableColumn<StockItem, String> colBrand;
    @FXML private TableColumn<StockItem, String> colCategory;
    @FXML private TableColumn<StockItem, Integer> colQuantity;
    @FXML private TableColumn<StockItem, Integer> colPrice;
	@FXML private Pagination pagination; 
	
    //Static table info 
    private static int RESULTS_PER_PAGE = 16; 
    public static final int TABLE_STATE_ALL = 1; 
    public static final int TABLE_STATE_SEARCH_BY_ID = 2; 
    public static final int TABLE_STATE_SEARCH_BY_BRAND = 3; 
    public static final int TABLE_STATE_SEARCH_BY_CATEGORY = 4; 
    public static final int TABLE_STATE_SEARCH_BY_PRICE = 5; 
    public static int TABLE_STATE = TABLE_STATE_ALL; 
    public static String SEARCH_STRING; 
    
    private StockItem activeItem; 
    
	public void initialize(){ 
		//Set table column values 
		colProductId.setCellValueFactory(new PropertyValueFactory<StockItem, Integer>("productId"));
		colBrand.setCellValueFactory(new PropertyValueFactory<StockItem, String>("brandName"));
		colCategory.setCellValueFactory(new PropertyValueFactory<StockItem, String>("categoryName"));        
		colQuantity.setCellValueFactory(new PropertyValueFactory<StockItem, Integer>("productQuantity"));        
		colPrice.setCellValueFactory(new PropertyValueFactory<StockItem, Integer>("sellPrice")); 
        
        pagination.currentPageIndexProperty().addListener((observableValue, oldValue, newValue) -> { 
        	showItems(RESULTS_PER_PAGE * (int) newValue); 
        }); 
        
        //Add product combobox sort by change handler 
        cbb_stock_search_by.getSelectionModel().selectedItemProperty().addListener((observableValue, oldValue, newValue) -> {
        	resetTable(); 
        	txt_stock_search.setVisible(true); 
        	hbox_price_details.setVisible(false); 
        	if(newValue.equals("id")){
        		txt_stock_search.setPromptText("Enter product id"); 
        	} else if (newValue.equals("brand")) {
        		txt_stock_search.setPromptText("Enter brand"); 
        	} else if (newValue.equals("category")){
        		txt_stock_search.setPromptText("Enter category"); 
        	}else if(newValue.equals("price")){
        		txt_stock_search.setPromptText("Enter min / max price");
        		txt_stock_search.setVisible(false);
        		hbox_price_details.setVisible(true);
        	}
        }); 
        
        //Init 
        resetTable(); 
	}
	
	/**
	 * Navigate to the dashboard 
	 * @param event
	 */
	@FXML void gotoDashboard(ActionEvent event) {
       VistaNavigator.loadVista(VistaNavigator.DASHBOARD);
	}
	
    /**
     * Reset the table with all stock items, without filter 
     */
	@FXML private void resetTable(){ 
		activeItem = null; 
		TABLE_STATE = TABLE_STATE_ALL; 
		setSearchText(""); 
		
		int count = StockItem.countAll(); 
		pagination.setPageCount(count / RESULTS_PER_PAGE + 1); 
		showItems(0); 
	}
	
	/**
	 * Manage search box key events 
	 * @param event
	 */
	@FXML public void onKeyReleasedSearchStock(KeyEvent event){ 
		if((event.getCode() == KeyCode.BACK_SPACE || event.getCode() == KeyCode.DELETE)){ 
			if(txt_stock_search.getText().length() == 0){ 
				resetTable(); 
			}
		}else if (event.getCode() == KeyCode.ENTER) {
			searchStock(); 
		} 
	} 
    
	/**
	 * Search the stock 
	 */
	@FXML public void searchStock(){
		String strSearch = txt_stock_search.getText().trim(); 
		String strSearchBy = ((String) cbb_stock_search_by.getSelectionModel().getSelectedItem().toString()).trim(); 
		
		SEARCH_STRING = strSearch; 
		
		switch(strSearchBy){ 
			
			case "category": 
				TABLE_STATE = TABLE_STATE_SEARCH_BY_CATEGORY; 
				break; 
				
			case "brand": 
				TABLE_STATE = TABLE_STATE_SEARCH_BY_BRAND; 
				break; 
		
			case "id": 
				TABLE_STATE = TABLE_STATE_SEARCH_BY_ID; 
				break; 
				
			case "price": 
				TABLE_STATE = TABLE_STATE_SEARCH_BY_PRICE; 
				break; 
		}
		showItems(0);
		selectTableRow(0); 
	}
	
	/**
	 * Public method to set searchbox text 
	 * @param strText
	 */
    public void setSearchText(String strText){
    	txt_stock_search.setText(strText); 
    }	
	
	/**
	 * Show products depending on state 
	 */
	public void showItems(int pageOffset){ 
		List<StockItem> list = null;  
		
		if(TABLE_STATE == TABLE_STATE_ALL){ 
			list = getStockList(pageOffset); 
			if(pageOffset == 0) pagination.setCurrentPageIndex(0); 
			
		} else if(TABLE_STATE == TABLE_STATE_SEARCH_BY_ID){ 
			int id = SEARCH_STRING.equals("") || !utils.TypeUtils.isNumeric(SEARCH_STRING) ? 0 : Integer.parseInt(SEARCH_STRING); 
			System.out.println(id);
			list = getStockListSearchById(id); 
			pagination.setPageCount(1); 
			if(list == null || list.size() == 0) lbl_placeholder_stock.setText("No results for '" + SEARCH_STRING + "'" ); 
			
		} else if(TABLE_STATE == TABLE_STATE_SEARCH_BY_BRAND){ 
			pagination.setPageCount(countSearchByBrandName(SEARCH_STRING) / RESULTS_PER_PAGE + 1); 
			list = getStockListSearchByBrandName(SEARCH_STRING, pageOffset); 
			if(list == null || list.size() == 0) lbl_placeholder_stock.setText("No results for '" + SEARCH_STRING + "'" ); 
			
		} else if(TABLE_STATE == TABLE_STATE_SEARCH_BY_CATEGORY){
			pagination.setPageCount(countSearchByCategoryName(SEARCH_STRING) / RESULTS_PER_PAGE + 1); 
			list = getStockListSearchByCategoryName(SEARCH_STRING, pageOffset); 
			if(list == null || list.size() == 0) lbl_placeholder_stock.setText("No results for '" + SEARCH_STRING + "'" ); 
			
		} else if (TABLE_STATE == TABLE_STATE_SEARCH_BY_PRICE){ 
			//This is imperfect, but time is limited for deeper validation 
			double max_price = utils.TypeUtils.isNumeric(txt_max_price.getText()) ? Double.parseDouble(txt_max_price.getText()) : 100000000; //Equals +infinite  
			double min_price = utils.TypeUtils.isNumeric(txt_min_price.getText()) ? Double.parseDouble(txt_min_price.getText()) : 100000000; 
			
			list = getStockListSearchByPrice(min_price, max_price, pageOffset); 
			pagination.setPageCount(countSearchByPrice(min_price, max_price) / RESULTS_PER_PAGE + 1); 
			if(list == null || list.size() == 0) lbl_placeholder_stock.setText("No results for products between " + min_price + " eur AND " + max_price + " eur."); 
		}
		
		if(list != null){ 
			stockTable.getItems().setAll(list); 
			
			//Select table row 
			int index = 0; 
			if(activeItem != null){ 
				int cnt = 0; 
				for (StockItem i : stockTable.getItems()){ 
	            	if(activeItem.getId() == i.getId()){ 
	            		index = cnt; 
	            		break; 
	            	}; 
	            	cnt++; 
				}
			} 
			selectTableRow(index); 
		}
	}
	
	/**
	 * Select a table row 
	 * @param index
	 */
	private void selectTableRow(int index){
		stockTable.requestFocus();
		stockTable.getSelectionModel().select(index); 
		stockTable.getFocusModel().focus(index);
	} 
	
	/**
	 * Delete client 
	 */
	@FXML private void deleteStockItem(){
    	boolean bDelete = new ConfirmationModal().show("Delete this item?", "Save client", "Yes", "No"); 
    	
    	if(bDelete){ 
			activeItem.delete(); 
			resetTable(); 
    	}
	}
		
	/**
	 * Fetch Stock items  
	 * @return
	 */
    private List<StockItem> getStockList(int pageOffset){ 
		List<StockItem> list = StockItem.fetchAll(RESULTS_PER_PAGE, pageOffset, "Stock.*, Product.sell_price as sell_price, Brand.id as brand_id, Category.id as category_id, Brand.name as brand_name, Category.name as category_name"); 
		return list; 
    }
    
    /**
     * Fetch stock item by id 
     * @param id
     * @return
     */
    private List<StockItem> getStockListSearchById(int id){ 
    	List<StockItem> list = new ArrayList<>(); 
    	StockItem i = new StockItem(); 
    	i.fetch(id); 
    	
    	Product product = new Product(); 
    	product.fetch(i.getProductId()); 
    	i.setCategory_name(product.getCategory_name());
    	i.setBrand_name(product.getBrand_name());
    	
    	if(i.getProductId() != 0) list.add(i); 
    	return list; 
    } 
    
    /**
     * Fetch stock items by brand name 
     * @param strSearch
     * @param pageOffset
     * @return
     */
    private List<StockItem> getStockListSearchByBrandName(String strSearch, int pageOffset){
    	List<StockItem> list = StockItem.fetchByBrandLike(strSearch, RESULTS_PER_PAGE, pageOffset, "Stock.*, Product.sell_price, Brand.id as brand_id, Category.id as category_id, Brand.name as brand_name, Category.name as category_name"); 
    	return list; 
    }
    
    /**
     * Fetch stock items by brand name 
     * @param strSearch
     * @param pageOffset
     * @return
     */
    private List<StockItem> getStockListSearchByCategoryName(String strSearch, int pageOffset){
    	List<StockItem> list = StockItem.fetchByCategoryLike(strSearch, RESULTS_PER_PAGE, pageOffset, "Stock.*, product.sell_price, Brand.id as brand_id, Category.id as category_id, Brand.name as brand_name, Category.name as category_name"); 
    	return list; 
    }
    
    /**
     * Get stock list by price 
     * @param min_price
     * @param max_price
     * @param pageOffset
     * @return
     */
	private List<StockItem> getStockListSearchByPrice(double min_price, double max_price, int pageOffset) {
    	List<StockItem> list = StockItem.fetchByPriceBetween(min_price, max_price, RESULTS_PER_PAGE, pageOffset, "Stock.*, product.sell_price, Brand.id as brand_id, Category.id as category_id, Brand.name as brand_name, Category.name as category_name"); 
    	return list; 
	}
    
    /**
     * Count all brands by name for pagination 
     * @param strSearch
     * @return
     */
    private int countSearchByBrandName(String strSearch){ 
		Predicate<String> p1 = new Predicate<>("Brand.name", "LIKE", "%" + strSearch + "%"); 
    	
    	Query q = new Query("select")
    					.selectFields("COUNT(*)")
    					.setTable(StockItem.tableName + " Stock")
    					.innerJoinOn(Product.tableName + " Product", "Stock.product_id = Product.id")
    					.innerJoinOn(Brand.tableName + " Brand", "Product.brand_id = Brand.id")
						.where(p1); 
    	
    	return Dao.countAll(q); 
    }
    
    /**
     * Count all categories by name for pagination 
     * @param strSearch
     * @return
     */
    private int countSearchByCategoryName(String strSearch){ 
		Predicate<String> p1 = new Predicate<>("Category.name", "LIKE", "%" + strSearch + "%"); 
    	
    	Query q = new Query("select")
    					.selectFields("COUNT(*)")
    					.setTable(StockItem.tableName + " Stock")
    					.innerJoinOn(Product.tableName + " Product", "Stock.product_id = Product.id")
    					.innerJoinOn(Category.tableName + " Category", "Product.category_id = Category.id")
						.where(p1); 
    	
    	return Dao.countAll(q); 
    }
    
    /**
     * Count all categories by name for pagination 
     * @param strSearch
     * @return
     */
    private int countSearchByPrice(double min_price, double max_price){ 
		Predicate<Double> p1 = new Predicate<>("Product.sell_price", "<=", max_price); 
		Predicate<Double> p2 = new Predicate<>("Product.sell_price", ">=", min_price); 
    	
    	Query q = new Query("select")
    					.selectFields("COUNT(*)")
    					.setTable(StockItem.tableName + " Stock")
    					.innerJoinOn(Product.tableName + " Product", "Stock.product_id = Product.id")
						.where(p1)
						.andWhere(p2); 
    	System.out.println(q);
    	System.out.println(Dao.countAll(q));
    	return Dao.countAll(q); 
    }    
	
}
