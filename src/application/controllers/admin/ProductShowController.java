package application.controllers.admin;

import javafx.fxml.FXML;
import javafx.scene.control.Label;
import pojos.Product;

public class ProductShowController {
	
    @FXML private Label lbl_id;
    @FXML private Label lbl_brand;
    @FXML private Label lbl_category;
    @FXML private Label lbl_sell_price; 
    
    public void initialize(){}
    
    /**
     * Set product on the controller 
     * @param product
     */
    public void setProduct(Product product){
	    if (product != null) {
	    	lbl_id.setText(Integer.toString(product.getId()));
	        lbl_brand.setText(product.getBrand_name());
	        lbl_category.setText(product.getCategory_name());
	        lbl_sell_price.setText(String.valueOf(product.getSell_price()) + " �");
	    } else { 
	    	lbl_id.setText("-");
	    }
    }
    
}
