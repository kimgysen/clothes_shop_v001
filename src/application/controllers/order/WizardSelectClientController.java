package application.controllers.order;

import java.util.ArrayList;
import java.util.List;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import pojos.Person;

/**
 * Select the client: this could be done by either scanning the client id, or looking up the client by name 
 * @author kimgysen
 *
 */
public class WizardSelectClientController {
	
	AddOrderController parentController; 
	
    @FXML private TableView<Person> clientsTable;
    @FXML private TableColumn<Person, String> colClientId;
    @FXML private TableColumn<Person, String> colFamilyName;
    @FXML private TableColumn<Person, String> colGivenName;
    
    @FXML private ComboBox<String> cbb_client_search_by; 
    @FXML private TextField txt_client_search; 
    @FXML private Button btn_client_search; 
	@FXML Button btn_client_search_by; 
    @FXML private Label lbl_placeholder_clients; 
    @FXML private Label lbl_selected_client; 
    
    //Static table info 
    public static final int TABLE_STATE_SEARCH_BY_NAME = 1; 
    public static final int TABLE_STATE_SEARCH_BY_ID = 2; 
    public static int TABLE_STATE = TABLE_STATE_SEARCH_BY_ID; 
    public static String SEARCH_STRING = ""; 
    
	@FXML Button btn_next; 
	
	public void initialize(){
		//Set table column values 
		colClientId.setCellValueFactory(new PropertyValueFactory<Person, String>("id"));
		colFamilyName.setCellValueFactory(new PropertyValueFactory<Person, String>("familyName"));
		colGivenName.setCellValueFactory(new PropertyValueFactory<Person, String>("givenName"));
		
        //Add client combobox sort by change handler 
        cbb_client_search_by.getSelectionModel().selectedItemProperty().addListener((observableValue, oldValue, newValue) -> {
        	resetTable(); 
        	if(newValue.equals("id")){
        		txt_client_search.setPromptText("Scan or enter client id"); 
        	} else if (newValue.equals("name")) { 
        		txt_client_search.setPromptText("Enter client name"); 
        	}
        }); 
        
        //Add table change listener clients 
        clientsTable.getSelectionModel().selectedItemProperty().addListener((observableValue, oldValue, newValue) -> {
            if (clientsTable.getSelectionModel().getSelectedItem() != null) { 
            	this.parentController.setClient(newValue);
            	lbl_selected_client.setText(newValue.getClientIdString()); 
    			btn_next.setDisable(false); 
            }
        }); 
        
	}
	
	
	//Set parent controllers for back navigation 
	/**
	 * Set the parent controller for wizard navigation 
	 * @param parentController
	 */
	public void setParentController(AddOrderController parentController){
		this.parentController = parentController; 
		//Disable "next" if client is not set 
		if(this.parentController.getClient() == null) btn_next.setDisable(true); 
	}
	
	/**
	 * Go to place orders 
	 */
	@FXML public void gotoPlaceOrders(){
		this.parentController.gotoPlaceOrders();
	}
	
	/**
	 * Manage search box key events 
	 * @param event
	 */
	@FXML public void onKeyReleasedSearchClient(KeyEvent event){ 
		if((event.getCode() == KeyCode.BACK_SPACE || event.getCode() == KeyCode.DELETE)){ 
			if(txt_client_search.getText().length() == 0){ 
				resetTable(); 
			}
		}else {
			searchClient(); 
			lbl_selected_client.setText("-");
			btn_next.setDisable(true); 
		}
	} 
	
	/**
	 * Reset the table 
	 */
	private void resetTable(){ 
		this.parentController.setClient(null); //Reset the client 
		searchClient(); 
		lbl_selected_client.setText("-");
		btn_next.setDisable(true);
	}
	
	/**
	 * Validate input + execute search 
	 */
	public void searchClient(){
		String strSearch = txt_client_search.getText().trim(); 
		String strSearchBy = (String) cbb_client_search_by.getSelectionModel().getSelectedItem().toString(); 
		SEARCH_STRING = strSearch; 
		
		switch(strSearchBy){ 
		
			case "name": 
				TABLE_STATE = TABLE_STATE_SEARCH_BY_NAME; 
				break; 
		
			case "id": 
				TABLE_STATE = TABLE_STATE_SEARCH_BY_ID; 
				break; 
		}
		showClients(0);
		lbl_selected_client.setText("-"); 
		btn_next.setDisable(true);
	}
	
    /**
     * Load client detail 'show' view 
     * @param client
     */
	private void showClients(int pageOffset) {
		List<Person> list = null;  
		
		String strPlaceholder = "No results for '" + SEARCH_STRING + "'"; 
		
		if(TABLE_STATE == TABLE_STATE_SEARCH_BY_NAME){ 
			if(SEARCH_STRING.equals("")) strPlaceholder = "Enter client name"; 
			list = getClientListSearchByName(SEARCH_STRING, pageOffset); 
			
		} else if(TABLE_STATE == TABLE_STATE_SEARCH_BY_ID){
			int id = 0; 
			if(SEARCH_STRING.equals("")){
				id = 0; 
				strPlaceholder = "Scan or enter client id"; 
			}else if(utils.TypeUtils.isNumeric(SEARCH_STRING)){
				id = Integer.parseInt(SEARCH_STRING); 
			}else {
				strPlaceholder = "Id must be numeric"; 
			}
			list = getClientListSearchById(id); 
		}
		
		if(list.size() == 0) lbl_placeholder_clients.setText(strPlaceholder); 
		clientsTable.getItems().setAll(list); 
		
	}
	
    /**
     * Fetch users by id 
     * @param id
     * @return
     */
    private List<Person> getClientListSearchById(int id){ 
    	List<Person> list = new ArrayList<>(); 
    	Person p = new Person(); 
    	p.fetch(id); 
    	if(p.getId() != 0) list.add(p); 
    	return list; 
    }
    
    /**
     * Fetch users by name 
     * @param strSearch
     * @param pageOffset
     * @return
     */
    private List<Person> getClientListSearchByName(String strSearch, int pageOffset){
    	List<Person> list = Person.fetchLike(strSearch, 100, pageOffset); 
    	return list; 
    }	
	
}
