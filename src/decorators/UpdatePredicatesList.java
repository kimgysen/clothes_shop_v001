package decorators;

import java.util.ArrayList;

import query.Predicate;

/**
 * A list that contains update predicates (set key = value) 
 * @author kimg
 *
 */
public class UpdatePredicatesList extends ArrayList<Predicate<?>>{
	
	private static final long serialVersionUID = 1L;
	
	@Override
	public boolean add(Predicate<?> e) {
		if(isPresent(e))	//Update to the latest predicate to avoid duplicate predicates on the same query 
			super.remove(e); 
		
		return super.add(e);
	}
	
	private boolean isPresent(Predicate<?> p){
		return this.contains(p); 
	}
	
}
