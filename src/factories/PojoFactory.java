package factories;

import pojos.Brand;
import pojos.Category;
import pojos.Order;
import pojos.Person;
import pojos.Product;
import pojos.StockItem;

public class PojoFactory {
	
	public static Object createPojo(String pojoName){ 
		
		Object obj = null; 
		
		switch(pojoName){ 
			case "Person": 
				obj = new Person(); 
				break; 
			
			case "Product": 
				obj = new Product(); 
				break; 
				
			case "Brand": 
				obj = new Brand(); 
				break; 
				
			case "Category": 
				obj = new Category(); 
				break; 
				
			case "StockItem": 
				obj = new StockItem(); 
				break; 
				
			case "Order": 
				obj = new Order(); 
				break; 
		}
		
		return obj;
		
	}
	
}
