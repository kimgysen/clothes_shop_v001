package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import query.Predicate;
import query.Query;
/**
 * Abstract class provides convenience methods for database connectivity 
 * @author kimg
 *
 */
public abstract class Model {
	
	/**
	 * Return a resultset based on a raw sql query string 
	 * @param strQuery
	 * @return
	 * @throws SQLException
	 */
	public static ResultSet rawGetActions(String strQuery){
		
		Connection conn = null; 
		Statement stmt = null; 
		ResultSet rs = null; 
		
		try {
			conn = Database.getConnection();
		} catch (SQLException e1) {
			e1.printStackTrace();
		} 
		
		try {
			stmt = Database.getConnection().createStatement();
			rs = stmt.executeQuery(strQuery); 
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				conn.close(); 
				rs.close();
				stmt.close(); 
			} catch (SQLException e) {
				e.printStackTrace();
			} 
			
		}
		return rs; 
	}
	
	/**
	 * Returns list with hashmaps for simple "select" statements (no parameter binding) 
	 * @return
	 * @throws SQLException
	 */
	public static List<Map<String, Object>> simpleGetActions(Query q){ 
		
		Connection conn = null; 
		Statement stmt = null; 
		ResultSet rs = null; 
		List<Map<String, Object>> list = null; 
		try {
			conn = Database.getConnection(); 
			stmt = conn.createStatement(); 
			rs = stmt.executeQuery(q.toString());
			list = resultSetToList(rs); 
			
		} catch (SQLException e) {
			e.printStackTrace();
		}finally{
			try {
				conn.close(); 
				rs.close();
				stmt.close(); 
			} catch (SQLException e) {
				e.printStackTrace();
			} 
		} 
		
		return list; 
	}
	
	/**
	 * Returns list with hashmaps for prepared "select" statements 
	 * @param q
	 * @return
	 * @throws SQLException
	 */
	public static List<Map<String, Object>> preparedGetActions(Query q){ 
		
		Connection conn = null; 
		PreparedStatement pstmt = null;
		ResultSet rs = null; 
		List<Map<String, Object>> list = null; 
		
		try {
			conn = Database.getConnection(); 
			pstmt = conn.prepareStatement(q.toString());
			setPreparedParameters(pstmt, q); 
			rs = pstmt.executeQuery(); 
			list = resultSetToList(rs); 
		} catch (SQLException e) {
			e.printStackTrace();
		} finally{
			try {
				conn.close(); 
				rs.close();
				pstmt.close(); 
				
			} catch (SQLException e) {
				e.printStackTrace();
			} 
		}
		
		return list; 
	}
	
	/**
	 * Performs "update" or "insert" with parameter binding 
	 * @param q
	 * @throws SQLException
	 * @return The newly created id 
	 */
	public static int preparedSetActions(Query q){
		
		Connection conn = null; 
		PreparedStatement pstmt = null;
		ResultSet rs = null; 
		
		int id = 0; 
		
		try {
			conn = Database.getConnection(); 
			pstmt = conn.prepareStatement(q.toString(), Statement.RETURN_GENERATED_KEYS);
			setPreparedParameters(pstmt, q); 
			pstmt.executeUpdate(); 
			
			if(q.getType().equals("INSERT")){
				rs = pstmt.getGeneratedKeys(); 
				rs.next(); 
				id = rs.getInt(1); 
			} else{
				id = 0; //Update doesn't require a return key 
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				conn.close(); 
				
			} catch (SQLException e) {
				e.printStackTrace();
			} 
		}
		return id; 
	}
	
	public static int preparedCountActions(Query q){ 
		
		Connection conn = null; 
		PreparedStatement pstmt = null;
		ResultSet rs = null; 
		
		
		int count = 0; 
		try {
			conn = Database.getConnection(); 
			pstmt = conn.prepareStatement(q.toString());
			setPreparedParameters(pstmt, q); 
			rs = pstmt.executeQuery(); 
			
		    while(rs.next()){
		    	count = rs.getInt(1);
		    }
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				conn.close(); 
				rs.close();
				pstmt.close(); 
				
			} catch (SQLException e) {
				e.printStackTrace();
			} 
		}
		return count; 
	}
	
	/**
	 * Find correct type + bind prepared statement parameters 
	 * @param pstmt
	 * @param q
	 * @return
	 * @throws SQLException
	 */
	private static PreparedStatement setPreparedParameters(PreparedStatement pstmt, Query q) throws SQLException{
		int cnt = 1; 
		
		for (Predicate<?> p : q.getPredicates()) { 
			//Extend further for other types (for the exercise, we keep it simple) 
			switch (p.getClassType().getSimpleName()){
				case "String": 
					pstmt.setString(cnt, (String) p.getOperand2());
					break; 
				case "Integer": 
					pstmt.setInt(cnt, (Integer) p.getOperand2());
					break; 
				case "Double": 
					pstmt.setDouble(cnt, (Double) p.getOperand2());
					break; 
			}
			cnt++; 
		}
		
		return pstmt; 
	}
	
	/**
	 * Turns resultSet into a List of HashMaps 
	 * @param rs
	 * @return
	 * @throws SQLException 
	 */
	private static List<Map<String, Object>> resultSetToList(ResultSet rs) throws SQLException{ 
		
		Map<String, Object> map = null; 
		List<Map<String, Object>> list = new ArrayList<>(); 
		
		//If empty 
		if(!rs.isBeforeFirst()){
			return null; 
		}
		
		//If not empty 
		while(rs.next()){ 
			//Extract types and cast 
			ResultSetMetaData rsmd = rs.getMetaData(); 
			Object fieldValue = null; 
			String fieldName = ""; 
			
			map = new HashMap<>(); 
			
			for (int i = 1; i <= rsmd.getColumnCount(); i++) { 
				fieldName = rsmd.getColumnLabel(i); 
				String type = rsmd.getColumnTypeName(i); 
				switch(type){ 
					case "CHAR":
					case "VARCHAR": 
						fieldValue = (String) rs.getString(fieldName); 
						break; 
				    case "INTEGER":
				    case "INT":
				    case "TINYINT":
				    case "SMALLINT":
				    	fieldValue = (int) rs.getInt(fieldName); 
				    	break; 
				    case "FLOAT":
				    	fieldValue = (float) rs.getInt(fieldName); 
				    	break; 
				    case "DECIMAL":
				    case "DOUBLE":
				    	fieldValue = (double) rs.getInt(fieldName); 
				    	break; 
				    case "TIMESTAMP": 
				    	fieldValue = ((Timestamp) rs.getTimestamp(fieldName)).toString(); 
				}
				map.put(fieldName, fieldValue); 
			}
			list.add(map); 
		}
		return list; 
	} 
	
}
