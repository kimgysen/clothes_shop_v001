package query;

public class Predicate<T> {
	
	private String operand1; 
	private String operator; 
	private T operand2; 
	private Class<T> classType; 
	
	public Predicate(String operand1, T operand2){
		this(operand1, "=", operand2); 
	}
	
	@SuppressWarnings("unchecked")
	public Predicate(String operand1, String operator, T operand2){
		this.operand1 = operand1; 
		this.operator = operator; 
		this.operand2 = operand2; 
		this.classType = (Class<T>) operand2.getClass(); 
	}
	
	public String getOperand1() {
		return operand1;
	}

	public String getOperator() {
		return operator;
	}
	
	public T getOperand2() {
		return operand2;
	}
	
	public Class<T> getClassType() {
		return classType;
	}
	
	@Override
	public boolean equals(Object obj) {
		Predicate<?> newObj = (Predicate<?>) obj; 
		return this.getOperand1().equals(newObj.getOperand1()); 
	}
	
	@Override
	public String toString() {
		return operand1 + " " + operator + " ?"; 
	}
	
	
}
