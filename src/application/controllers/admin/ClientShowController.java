package application.controllers.admin;

import enums.Gender;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import pojos.Person;

public class ClientShowController {
	
    @FXML private Label lbl_id;
    @FXML private Label lbl_given_name;
    @FXML private Label lbl_family_name;
    @FXML private Label lbl_email;
    @FXML private Label lbl_address;
    @FXML private Label lbl_zip;
    @FXML private Label lbl_city;
    @FXML private Label lbl_gender;
    @FXML private Label lbl_phone;
    
    @FXML private Button btn_showOrders; 
    
    ClientsController parentController; 
    
    public void initialize(){}
    
    public void setParentController(ClientsController parentController){
    	this.parentController = parentController; 
    }
    
    /**
     * Set the client 
     * @param client
     */
    public void setClient(Person client){
	    if (client != null) {
	    	lbl_id.setText(Integer.toString(client.getId()));
	    	lbl_given_name.setText(client.getGivenName());
	    	lbl_family_name.setText(client.getFamilyName());
	    	lbl_gender.setText(Gender.getGenderAsString(client.getGender()));
	    	lbl_email.setText(client.getEmail());
	    	lbl_address.setText(client.getAddress());
	    	lbl_zip.setText(client.getPostal_code());
	    	lbl_city.setText(client.getCity());
	    	lbl_phone.setText(client.getPhone());
	        
	    } else {
	    	lbl_id.setText("-");
	    	lbl_given_name.setText("-");
	    	lbl_family_name.setText("-");
	    	lbl_gender.setText("-");
	    	lbl_email.setText("-");
	    	lbl_address.setText("-");
	    	lbl_zip.setText("-");
	    	lbl_city.setText("-");
	    	lbl_phone.setText("-");
	    }
    }
    
    /**
     * Show client orders 
     */
    @FXML public void showOrders(){
    	parentController.showClientOrders();
    }
    
}
