package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import config.DbConf;

public class Database { 
	
	/**
	 * Return a connection 
	 * @return
	 * @throws SQLException
	 */
	public static Connection getConnection() throws SQLException{ 
		Connection conn = DriverManager.getConnection(DbConf.getUrl(), DbConf.getUser(), DbConf.getPassword()); 
		return conn; 
	}
}
