package pojos;

import java.util.List;

import dao.Dao;
import dao.IDao;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import query.Predicate;
import query.Query;

public class Person extends Dao<Person> implements IDao<Person> { 
	
	private int id; 
	private int gender; 
	private final StringProperty givenName; 
	private final StringProperty familyName; 
	private String address; 
	private String postalCode; 
	private String city; 
	private String countryCode; 
	private String email; 
	private String phone; 
	
	public static String tableName = "client"; 
	
	public Person(){
		this(null, null, 0); 
	}; 
	
	//public Person(int id){} 
	public Person(String givenName, String familyName, int gender){
		super("id", "given_name", "family_name", "gender", "address", "postal_code", "city", "country_code", "email", "phone"); 
		this.givenName = new SimpleStringProperty(givenName);
		this.familyName = new SimpleStringProperty(familyName);
		this.setGender(gender); 
	}
	
	public static int countAll(){
		Query q = new Query("select")
						.selectFields("COUNT(*)")
						.setTable(tableName); 
		
		int count = Dao.countAll(q); 
		
		return count; 
	} 
	
	/**
	 * Get a list of persons based on a query string
	 * @param fieldNames
	 * @return List of Persons
	 */
	public static List<Person> fetchAll(int limit, int offset, String... fieldNames){ 
		
		//Specify custom query 
		Query q = new Query("select")
						.selectFields(fieldNames) 
						.setTable(tableName)
						.orderBy("family_name", "given_name")
						.limit(limit)
						.offset(offset); 
		
		List<Person> list = Dao.fetchAll(Person.class, q); 
		return list; 
	} 
	
	public static List<Person> fetchLike(String strLike, int limit, int offset, String ...fieldNames){
		Predicate<String> p1 = new Predicate<>("given_name", "LIKE", "%" + strLike + "%"); 
		Predicate<String> p2 = new Predicate<>("family_name", "LIKE", "%" + strLike + "%"); 
		Predicate<String> p3 = new Predicate<>("CONCAT(given_name, ' ', family_name)", "LIKE", "%" + strLike + "%"); //Concatenate to fullname
		Predicate<String> p4 = new Predicate<>("CONCAT(family_name, ' ', given_name)", "LIKE", "%" + strLike + "%"); //Inverse
		//Specify custom query 
		Query q = new Query("select")
						.selectFields(fieldNames) 
						.setTable(tableName)
						.where(p1)
						.orWhere(p2)
						.orWhere(p3)
						.orWhere(p4)
						.orderBy("family_name", "given_name"); 
		
		if(limit != 0){
			q.limit(limit); 
		}
		if(offset != 0){
			q.offset(offset); 
		}
		
		List<Person> list = Dao.fetchAll(Person.class, q); 
		return list; 
		
	}
	
	public void fetch(int id){
		Person p = Dao.fetch(Person.class, tableName, id); 
		//Set all properties on the current object 
		if(p != null){
			this.setId(id);
			this.setGivenName(p.getGivenName());
			this.setFamily_name(p.getFamilyName());
			this.setEmail(p.getEmail());
			this.setGender(p.getGender());
			this.setAddress(p.getAddress());
			this.setPostal_code(p.getPostal_code());
			this.setCity(p.getCity());
			this.setCountry_code(p.getCountry_code());
			this.setPhone(p.getPhone()); 
		}

	}
	
	public void save(){ 
		int id = super.save(tableName); 
		if( id > 0 ) this.id = id; 
	}
	
	public void delete(){
		super.delete(tableName);
	}
		
	public int getId() { 
		return id;
	}
	
	public void setId(int id) { 
		this.setHasId(true, id); 
		this.id = id; 
	} 
	
	public int getGender() {
		return gender;
	}
	
	public void setGender(int gender) {
		super.setDirty(true, "gender", gender, Integer.class); 
		this.gender = gender;
	}
	
	public void setGiven_name(String givenName){ 
		this.setGivenName(givenName); 
	}
	
	public String getGivenName() {
		return givenName.get();
	}
	
    public void setGivenName(String givenName) {
    	super.setDirty(true, "given_name", givenName, String.class); 
        this.givenName.set(givenName);
    }
	
	public StringProperty givenNameProperty() {
        return givenName;
    }
	
	public void setFamily_name(String familyName){ 
		this.setFamilyName(familyName); 
	}
	
	public String getFamilyName() {
		return familyName.get();
	}
	
    public void setFamilyName(String familyName) {
    	super.setDirty(true, "family_name", familyName, String.class); 
        this.familyName.set(familyName);
    }
	
	public StringProperty familyNameProperty() {
        return familyName;
    }
	
	public String getClientIdString(){
		return this.getId() + " - " + this.getFamilyName() + " " + this.getGivenName();
	}
	
	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		super.setDirty(true, "address", address, String.class); 
		this.address = address;
	}

	public String getPostal_code() {
		return postalCode;
	}
	
	public void setPostal_code(String postal_code) {
		super.setDirty(true, "postal_code", postal_code, String.class); 
		this.postalCode = postal_code;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		super.setDirty(true, "city", city, String.class); 
		this.city = city;
	}

	public String getCountry_code() {
		return countryCode;
	}

	public void setCountry_code(String country_code) {
		super.setDirty(true, "country_code", country_code, String.class); 
		this.countryCode = country_code;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		super.setDirty(true, "email", email, String.class); 
		this.email = email;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		super.setDirty(true, "phone", phone, String.class); 
		this.phone = phone;
	}
	
	@Override
	public String toString() {
		return "Person [id=" + id + ", gender=" + gender + ", given_name=" + givenName + ", familyName=" + familyName
				+ ", address=" + address + ", postalCode=" + postalCode + ", city=" + city + ", countryCode="
				+ countryCode + ", email=" + email + ", phone=" + phone + "]";
	}
	
}
